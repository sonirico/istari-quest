package com.katodia.istariquest.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.katodia.istariquest.G;
import com.katodia.istariquest.IstariQuest;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = G.UI.WIDTH;
		config.height = G.UI.HEIGHT;
		config.title = G.UI.TITLE;
		config.resizable = G.UI.RESIZABLE;
		config.foregroundFPS = 30;
		config.backgroundFPS = 10;
		config.fullscreen = G.UI.FULL_SCREEN;
		//config.forceExit = true;
		new LwjglApplication(new IstariQuest(), config);
	}
}
