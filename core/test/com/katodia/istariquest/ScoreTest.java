package com.katodia.istariquest;

import com.artemis.Entity;
import com.artemis.World;
import com.artemis.WorldConfigurationBuilder;
import com.katodia.istariquest.components.ScoreComponent;
import com.katodia.istariquest.components.hordes.HordeComponent;
import com.katodia.istariquest.systems.ScoreSystem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ScoreTest {

    @Test
    public void multiple_score_test () {
        ScoreSystem ss = new ScoreSystem();
        World world = new World(new WorldConfigurationBuilder().with(ss).build());

        Entity e1 = world.createEntity();
        e1.edit().create(ScoreComponent.class).setPoints(10);

        Entity e2 = world.createEntity();
        e2.edit().create(ScoreComponent.class).setPoints(12);

        Entity e3 = world.createEntity();
        e3.edit().create(HordeComponent.class);
        e3.edit().create(ScoreComponent.class).setPoints(15);

        ss.collectScore(e1.getId(), G.ENEMY_TYPES.SOLE_ENEMY);

        assertEquals(10, ss.getTotalScore());

        ss.collectScore(e2.getId(), G.ENEMY_TYPES.SOLE_ENEMY);

        assertEquals(22, ss.getTotalScore());

        ss.collectScore(e3.getId(), G.ENEMY_TYPES.SOLE_ENEMY);

        assertEquals(37, ss.getTotalScore());
    }
}
