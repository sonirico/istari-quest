package com.katodia.istariquest;


import com.artemis.*;
import com.artemis.utils.Bag;
import com.badlogic.gdx.math.Vector2;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.utils.SortingUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ZindexTest {

	private static World world;

	@BeforeClass
	public static void beforeClass () {
		world = new World(new WorldConfiguration());
		world.inject(new SortingUtils());
	}

	@Test
	public void ordered_by_yaxis_test () {

		Bag<Entity> unsortedBag = new Bag<>();

		Entity e1 = world.createEntity();
		e1.edit().create(ActorComponent.class).setPosition(new Vector2(0, 1000));
		int a1Id = e1.getId();

		Entity e2 = world.createEntity();
		e2.edit().create(ActorComponent.class).setPosition(new Vector2(0, 100));
		int a2Id = e2.getId();


		unsortedBag.add(e1);
		unsortedBag.add(e2);


		world.process();

		unsortedBag = SortingUtils.sortByAxis(unsortedBag);

		assertEquals(a1Id,  unsortedBag.get(0).getId());
		assertEquals(a2Id, unsortedBag.get(1).getId());

		world.getMapper(ActorComponent.class).get(e1).setPosition(new Vector2(0, 100));
		world.getMapper(ActorComponent.class).get(e2).setPosition(new Vector2(0, 1000));

		world.process();

		unsortedBag = SortingUtils.sortByAxis(unsortedBag);

		assertEquals(a1Id,  unsortedBag.get(1).getId());
		assertEquals(a2Id, unsortedBag.get(0).getId());
	}
}
