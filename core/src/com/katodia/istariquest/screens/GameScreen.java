package com.katodia.istariquest.screens;

import com.artemis.*;
import com.artemis.io.JsonArtemisSerializer;
import com.artemis.io.SaveFileFormat;
import com.artemis.link.EntityLinkManager;
import com.artemis.managers.GroupManager;
import com.artemis.managers.PlayerManager;
import com.artemis.managers.TagManager;
import com.artemis.managers.WorldSerializationManager;
import com.badlogic.gdx.*;
import com.badlogic.gdx.ai.msg.MessageManager;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.msg.Telegraph;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.IntMap;
import com.katodia.istariquest.AssetsHolder;
import com.katodia.istariquest.G;
import com.katodia.istariquest.IstariQuest;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.RectangleCollidableComponent;
import com.katodia.istariquest.scripts.playable.WalkScript;
import com.katodia.istariquest.systems.*;
import com.katodia.istariquest.systems.hordes.HordeSystem;
import com.katodia.istariquest.systems.hordes.TimeHordeSystem;
import com.katodia.istariquest.systems.input.PlayerInputSystem;
import com.katodia.istariquest.systems.input.VirtualBoard;
import com.katodia.istariquest.systems.render.HealthBarRenderSystem;
import com.katodia.istariquest.systems.render.RenderScoreSystem;
import com.katodia.istariquest.systems.states.common.DyingStateSystem;
import com.katodia.istariquest.utils.EntityFactory;
import com.katodia.istariquest.utils.SortingUtils;
import com.katodia.istariquest.utils.StageBuilder;
import se.feomedia.orion.system.OperationSystem;

import java.io.StringWriter;

public class GameScreen implements Screen, Telegraph {

    private final IstariQuest game;

    protected TiledMap map;

    protected TiledMapTileLayer groundLayer;
    protected MapObjects objectsInLayer;

    protected MapProperties properties;

    protected OrthographicCamera tileMapCamera;
    protected OrthographicCamera uiCamera;

    private InputMultiplexer multiplexer;

    private IntMap<VirtualBoard> virtualBoardMap; // Board for the main character

    private OrthogonalTiledMapRenderer tiledMapRenderer;

    private ShapeRenderer shapeRenderer;

    private PlayerInputSystem playerInputSystem;
    private ScrollSystem scrollSystem;
    private RenderSystem renderSystem;

    private World world;

    private Rectangle gameBounds;

    public GameScreen(IstariQuest game) {
        Gdx.app.setLogLevel(Application.LOG_INFO);
        this.game = game;


        this.shapeRenderer = new ShapeRenderer();
        this.gameBounds = new Rectangle();

        this.tileMapCamera = new OrthographicCamera();
        this.uiCamera = new OrthographicCamera();

        this.tileMapCamera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        this.uiCamera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        game.viewport.setCamera(uiCamera);
    }

    @Override
    public void show() {
        this.map = AssetsHolder.get("maps/new_stage1.tmx");


        this.gameBounds.set(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());


        this.game.batch.setProjectionMatrix(uiCamera.combined);
        this.tiledMapRenderer = new OrthogonalTiledMapRenderer(map);
        this.virtualBoardMap = new IntMap<>();

        this.properties = this.map.getProperties();
        this.groundLayer = (TiledMapTileLayer) map.getLayers().get("Ground");

        this.objectsInLayer = map.getLayers().get("Objects").getObjects();


        MapProperties mps = this.objectsInLayer.get("StartPosition").getProperties();

        //System.out.println(json.toJson(malo));
        WorldSerializationManager worldSerializationManager = new WorldSerializationManager();
        WorldConfiguration worldConfiguration = new WorldConfigurationBuilder()
                .with(worldSerializationManager)
                .with((scrollSystem = new ScrollSystem(tileMapCamera)).setDirection(new Vector2(-1, 0)))
                .with(new CollisionSystem(shapeRenderer))
                .with(new AnimationSystem())
                .with((renderSystem = new RenderSystem()).setSpriteBatch(game.batch))
                .with(new MovementSystem())
                .with(new WeaponSystem())
                .with(new SpawnSystem())
                .with(new CullingSystem(.1f, gameBounds))
                .with(new RemoveCulledSystem(1))
                .with(new MotionFlipSystem())
                .with(new ManualFlipSystem())
                .with(new MeleeCollisionSystem())
                .with(new CustomOperationSystem())
                .with(new HealthBarRenderSystem(shapeRenderer))
                .with(new RenderScoreSystem().setBatch(game.batch).setFont(game.font))
                .with(new DamageSystem())
                .with(new DeathSystem())
                .with(new HordeSystem())
                .with(new TimeHordeSystem())
                .with(new ScoreSystem())
                .with(new LimitPlayerBoundSystem())
                .with(new ScriptSystem())
                // Meta
                .with(new GroupManager())
                .with(new TagManager())
                .with(new PlayerManager())
                .with(new OperationSystem())
                .with(new EntityLinkManager())
                // States
                .with(new DyingStateSystem())
                .build();


        world = new World(worldConfiguration);

        // Injections
        world.inject(new SortingUtils());
		world.inject((playerInputSystem = new PlayerInputSystem()));

        worldSerializationManager.setSerializer(new JsonArtemisSerializer(world));

        setUpWorldCollisions();

        VirtualBoard virtualBoardPlayerOne = new VirtualBoard();
        VirtualBoard.VirtualBoardConfiguration defaultVirtualConf = new VirtualBoard.VirtualBoardConfiguration();

        defaultVirtualConf.setKeyMap(VirtualBoard.VirtualBoardConfiguration.defaultKeyMap);
        virtualBoardPlayerOne.setVirtualBoardConfiguration(defaultVirtualConf);

        Entity player = EntityFactory.spawnPlayer(world, virtualBoardPlayerOne, mps);

        virtualBoardMap.put(player.getId(), virtualBoardPlayerOne);


		multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(playerInputSystem);


        playerInputSystem.addPlayer(player.getId());

        Gdx.input.setInputProcessor(multiplexer);
        
        if (G.gameMode.equals("development")) {
            //StageLoader.load(world, 1);
            StageBuilder sb = new StageBuilder(world);
            sb.setUpStageOne();
            //StageSaver.saveStage(world, 1);
        } else {
			world.getSystem(HealthBarRenderSystem.class).setEnabled(false);
		}

        // Initializes EntityFactory
        EntityFactory.setWorld(world);

        Gdx.app.log("Game Screen", "show");
    }



    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        uiCamera.update();
        tileMapCamera.update();

        // Render some lines for debug purposes

        if (G.DEBUG) {
            this.shapeRenderer.setProjectionMatrix(this.uiCamera.combined);
            this.shapeRenderer.begin(ShapeRenderer.ShapeType.Line);

            this.shapeRenderer.setColor(Color.YELLOW);
            this.shapeRenderer.line(-1000, 0, 1000, 0);
            this.shapeRenderer.line(0, -1000, 0, 1000);

            for (int i = -1000; i <= 1000; i += 50) {
                this.shapeRenderer.line(-50, i, 50, i);
                this.shapeRenderer.line(i, -50, i, 50);
            }
            this.shapeRenderer.end();
        }

        this.tiledMapRenderer.setView(this.tileMapCamera);
        this.tiledMapRenderer.render();

        world.setDelta(delta);
        world.process();

        game.batch.setProjectionMatrix(uiCamera.combined);

        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            this.scrollSystem.setDirection(new Vector2(-1, 0));
        } else if (Gdx.input.isKeyPressed(Input.Keys.W)) {
            this.scrollSystem.setDirection(new Vector2(0, 1));
        } else if (Gdx.input.isKeyPressed(Input.Keys.S)) {
            this.scrollSystem.setDirection(new Vector2(0, -1));
        } else if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            this.scrollSystem.setDirection(new Vector2(1, 0));
        }else if (Gdx.input.isKeyPressed(Input.Keys.Z)) {
            //this.tileMapCamera.zoom += .01;
            //this.tileMapCamera.translate(0, 1);
        } else if (Gdx.input.isKeyPressed(Input.Keys.X)) {
            //this.tileMapCamera.zoom -= .01;
            //resize(800, 600);
            tileMapCamera.setToOrtho(false, 480, 320);
            uiCamera.setToOrtho(false, 480, 320);

        } else if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            Gdx.app.exit();
        }

        if (Gdx.input.isKeyPressed(Input.Keys.S)) {
            final EntitySubscription allEntities = world.getSystem(AspectSubscriptionManager.class).get(Aspect.all());

            try {
                final StringWriter writer = new StringWriter();
                final SaveFileFormat save = new SaveFileFormat(allEntities.getEntities());
                //world.getSystem(WorldSerializationManager.class).save(writer, save);

                final Preferences preferences = Gdx.app.getPreferences("MyGame");
                preferences.putString("MyStateId", writer.toString());
                preferences.flush();
            } catch (Exception e) {
                Gdx.app.error("MyGame", "Save Failed", e);
            }
        }


    }

    @Override
    public void resize(int width, int height) {
        Gdx.app.log("Game Screen Resize:", "New width: " + width + " New height: " + height);

        this.tileMapCamera.setToOrtho(false, G.UI.WIDTH, G.UI.HEIGHT);
        this.tileMapCamera.update();

        this.uiCamera.setToOrtho(false, G.UI.WIDTH, G.UI.HEIGHT);
        this.uiCamera.update();

        this.gameBounds.set(0, 0, width, height);
        //world.getSystem(CullingSystem.class).setCullingArea(gameBounds);
    }

    @Override
    public void pause() {
        for (BaseSystem bs: world.getSystems()){
            bs.setEnabled(false);
        }
        Gdx.app.log("Game Screen", "pause");
    }

    @Override
    public void resume() {
        for (BaseSystem bs: world.getSystems()){
            bs.setEnabled(true);
        }
        Gdx.app.log("Game Screen", "resume");
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        Gdx.app.log("Game Screen", "dispose");
        virtualBoardMap.clear();
        virtualBoardMap = null;
        map.dispose();
        //multiplexer.clear();
        MessageManager.getInstance().clear();
    }

    @Override
    public boolean handleMessage(Telegram msg) {
        return false;
    }


    private void setUpWorldCollisions () {
        // Create rectangle list from tile
        MapObjects collisions = map.getLayers().get("Collisions").getObjects();

        for (MapObject mo : collisions){

            // TODO: Encapsulate this in another class
            if (mo instanceof RectangleMapObject) {

                RectangleMapObject rmo = (RectangleMapObject)mo;

                Entity worldRectangle = world.createEntity();
                worldRectangle.edit().create(RectangleCollidableComponent.class);
                ActorComponent ac = worldRectangle.edit().create(ActorComponent.class);
                ac.setBounds(rmo.getRectangle());
                //this.engine.addEntity(
                //new Entity().add(new RectangleCollidableComponent(rmo.getRectangle()))
                //);
                world.getSystem(GroupManager.class).add(worldRectangle, G.GROUPS.COLLIDABLE_ELEMENT);
            }
        }
    }
}

