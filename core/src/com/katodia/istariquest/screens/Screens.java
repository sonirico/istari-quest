package com.katodia.istariquest.screens;


public enum Screens {
    GAME,
    MAINMENU,
    ABOUT,
    LICENCE,
    PAUSE
}
