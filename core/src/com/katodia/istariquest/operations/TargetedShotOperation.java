package com.katodia.istariquest.operations;

import com.artemis.ComponentMapper;
import com.artemis.annotations.EntityId;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.MotionComponent;
import com.katodia.istariquest.utils.VectorUtils;
import se.feomedia.orion.Executor;
import se.feomedia.orion.OperationTree;
import se.feomedia.orion.operation.SingleUseOperation;

public class TargetedShotOperation extends SingleUseOperation {

    public int velModule = 0;

    @EntityId public int targetId = -1;

    @Override
    public Class<? extends Executor> executorType() {
        return TargetedShotExecutor.class;
    }

    @Override
    public void reset() {
        super.reset();
        targetId = -1;
        velModule = 0;
    }

    @Wire
    public static class TargetedShotExecutor extends SingleUseExecutor<TargetedShotOperation>  {

        private ComponentMapper<ActorComponent> actorMapper;
        private ComponentMapper<MotionComponent> motionMapper;

        @Override
        protected void act(TargetedShotOperation op, OperationTree node) {
            if (! actorMapper.has(op.targetId)) {
                // Target has died
                return;
            }


            Vector2 newVel = VectorUtils.vectorBetween(
                    actorMapper.get(op.targetId).getCenteredPosition(),
                    actorMapper.get(op.entityId).getCenteredPosition()
            );

            float angle = newVel.angle();

            newVel.x = op.velModule * MathUtils.cos(angle * MathUtils.degreesToRadians);
            newVel.y = op.velModule * MathUtils.sin(angle * MathUtils.degreesToRadians);

            motionMapper.get(op.entityId).set(newVel);
        }
    }
}
