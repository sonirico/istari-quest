package com.katodia.istariquest.operations;

import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.katodia.istariquest.components.states.MeleeState;
import se.feomedia.orion.Executor;
import se.feomedia.orion.OperationTree;
import se.feomedia.orion.operation.SingleUseOperation;

public class MeleeAttackOperation extends SingleUseOperation{

    public int damage = 0;

    @Override
    public void reset() {
        super.reset();
        damage = 0;
    }

    @Override
    public Class<? extends Executor> executorType() {
        return MeleeExecutor.class;
    }

    @Wire
    public static class MeleeExecutor extends SingleUseExecutor<MeleeAttackOperation> {

        private ComponentMapper<MeleeState> mapper;

        @Override
        protected void act(MeleeAttackOperation op, OperationTree node) {
            mapper.create(op.entityId).setDamage(op.damage);
        }
    }
}