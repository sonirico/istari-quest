package com.katodia.istariquest.operations;


import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.katodia.istariquest.components.weapons.Weapon;
import com.katodia.istariquest.operations.movements.*;
import se.feomedia.orion.Operation;

import static se.feomedia.orion.OperationFactory.operation;

public final class CustomOperationFactory {
    private CustomOperationFactory() {
    }


    /**
     * Sets a linear motion movement
     * @param vel
     * @return
     */

    public static LinearMoveByVelocityOperation move (Vector2 vel) {
        LinearMoveByVelocityOperation lmbv = operation(LinearMoveByVelocityOperation.class);
        lmbv.vel.set(vel);
        return lmbv;
    }

    /**
     * Sets a linear motion movement specified by final destination and duration
     * @param deltaSpace the distance to move
     * @param duration how long it takes to reach it
     * @return
     */

    public static LinearMoveOperation moveBy(Vector2 deltaSpace, float duration) {
        LinearMoveOperation lmo = operation(LinearMoveOperation.class);
        lmo.vel.set(deltaSpace);
        lmo.duration = duration;
        return lmo;
    }

    /**
     * Sets harmonic motion
     * @param A total displacement
     * @param nLaps period (aka T)
     * @param angle initial angle (aka phi)
     * @param duration
     * @param vel velocity for the no harmonic axis
     * @param xAxis whether perform it in the x axis
     * @param yAxis whether perform it in the y axis
     * @return
     */

    public static SinusMoveOperation sinusMove (
        float A, float nLaps, float angle, float duration, Vector2 vel, boolean xAxis, boolean yAxis
    ) {
        SinusMoveOperation smo = operation(SinusMoveOperation.class);
        smo.vel.set(vel);
        smo.xAxis = xAxis;
        smo.yAxis = yAxis;
        smo.duration = duration;
        smo.A = A;
        smo.phi = angle * MathUtils.degreesToRadians;
        float f = nLaps / duration; // frequency = laps per second
        smo.omega = MathUtils.PI2 * f;// w = 2 * PI * frequency
        return smo;
    }

    /**
     * An alternate sinusoidal movement with no complex harmonic ecuations
     * @param vel
     * @param radius
     * @param xAxis
     * @param yAxis
     * @param duration
     * @return
     */

    public static AlternateSinusMoveOperation alternateSinusMove (Vector2 vel, float radius, boolean xAxis, boolean yAxis, float duration) {
        AlternateSinusMoveOperation smo = operation(AlternateSinusMoveOperation.class);
        smo.radius = radius;
        smo.xAxis = xAxis;
        smo.yAxis = yAxis;
        smo.vel.set(vel);
        smo.duration = duration;
        return smo;
    }

    /**
     * Chases the target. This movement takes as long as duration is set.
     * For a standard duration value, this movement will perform faster/lower
     * depending on the distance between the pursuer and the target
     * @param entityId the target to pursue
     * @param duration how long it takes to perform the movement.
     * @return
     */

    public static ChaseByDurationOperation chaseByDuration (int entityId, float duration) {
        ChaseByDurationOperation co = operation(ChaseByDurationOperation.class);
        co.targetEntityId = entityId;
        co.duration = duration;
        return co;
    }

    /**
     * Always chases the target with the given velocity
     * @param entityId
     * @param vel
     * @return
     */

    public static ChaseByVelocityOperation chaseByVelocity (int entityId, Vector2 vel) {
        ChaseByVelocityOperation co = operation(ChaseByVelocityOperation.class);
        co.targetId = entityId;
        co.vel.set(vel);
        return co;
    }

    public static AttackOperation attack (Weapon w) {
        AttackOperation ao = operation(AttackOperation.class);
        ao.weapon = w;
        return ao;
    }

    /**
     * Flips the sprite of an animated component
     * @param x whether flip on x axis
     * @param y whether flip on y axis
     * @return
     */

    public static FlipOperation flip (boolean x, boolean y) {
        FlipOperation fo = operation(FlipOperation.class);
        fo.flipX = x;
        fo.flipY = y;
        return fo;
    }

    /**
     * Makes and entity collidable with entities composed by DamageableComponents.
     * @param damage How much damage it will inflict
     * @return The operation to process
     */

    public static MeleeAttackOperation melee (int damage) {
        MeleeAttackOperation mao = operation(MeleeAttackOperation.class);
        mao.damage = damage;
        return mao;
    }

    /**
     * Removes the melee state from an entity
     * @return The operation to process
     */

    public static EndMeleeOperation endMelee() {
        EndMeleeOperation mao = operation(EndMeleeOperation.class);
        return mao;
    }

    /**
     * Sets animations for entities
     * @param animation
     * @return
     */

    public static AnimateOperation animate (Enum animation) {
        AnimateOperation ao = operation(AnimateOperation.class);
        ao.animation = animation;
        return ao;
    }

    public static ShotOperation shot (Weapon weapon) {
        ShotOperation so = operation(ShotOperation.class);
        so.weapon = weapon;
        return so;
    }

    public static ShotOperation shot (Weapon weapon, Operation moveDescriptor) {
        ShotOperation so = operation(ShotOperation.class);
        so.weapon = weapon;
        so.moveDescriptor = moveDescriptor;
        return so;
    }

    public static ShotOperation shot (Weapon weapon, Operation moveDescriptor, String... groups) {
        ShotOperation so = operation(ShotOperation.class);
        so.weapon = weapon;
        so.moveDescriptor = moveDescriptor;
        so.groups = groups;
        return so;
    }

    /**
     * fires a bullet targeting an entity
     * @param targetId Target entity id
     * @param velModule How fast will it move
     * @return
     */

    public static TargetedShotOperation targetedShot ( int targetId, int velModule ) {
        TargetedShotOperation tso = operation(TargetedShotOperation.class);
        tso.velModule = velModule;
        tso.targetId = targetId;
        return tso;
    }

    /**
     * Rotates entities to make them facing to the direction specified
     * @param direction
     * @return
     */

    public static RotateOnceOperation rotateOnce ( Vector2 direction ) {
        RotateOnceOperation roo = operation(RotateOnceOperation.class);
        roo.direction.set(direction);
        return roo;
    }

    /**
     * Rotates entities according to their motion component
     * @return
     */

    public static RotateByVelOperation rotateByVel () {
        return operation(RotateByVelOperation.class);
    }

    /**
     * Achieves the fancy effect of blinking.
     * @param minorA Low alpha
     * @param majorA High alpha
     * @param delay how long it takes to start blinking
     * @param interval Delta time between blinking
     * @param repeatCount How many times the Entity will blink
     * @return
     */

    public static BlinkOperation blink (
            float minorA, float majorA,
            float delay, float interval, int repeatCount)
    {
        BlinkOperation bo = operation(BlinkOperation.class);
        bo.minorAlpha = minorA;
        bo.minorAlpha = majorA;
        bo.delay = delay;
        bo.interval = interval;
        bo.repeatCount = repeatCount;
        return bo;
    }

    /**
     * Constantly chases the target by calculating the vector every iteration
     * @param entityId target ID
     * @param vel Module, how fast
     * @param duration how long will it chase
     * @return
     */

    public static HarassOperation harass (int entityId, int vel, float duration) {
        HarassOperation ho = operation(HarassOperation.class);
        ho.duration = duration;
        ho.targetId = entityId;
        ho.velocity = vel;
        return ho;
    }

    public static RotateTargetedOperation rotate_by_pos (int entityId, int vel) {
    	RotateTargetedOperation rto = operation(RotateTargetedOperation.class);
    	rto.targetId = entityId;
    	rto.vel = vel;
    	rto.duration = Integer.MAX_VALUE;
    	return rto;
	}
}