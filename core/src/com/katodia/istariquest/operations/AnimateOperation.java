package com.katodia.istariquest.operations;

import com.artemis.ComponentMapper;
import com.katodia.istariquest.components.AnimatedComponent;
import se.feomedia.orion.Executor;
import se.feomedia.orion.OperationTree;
import se.feomedia.orion.operation.SingleUseOperation;

public class AnimateOperation extends SingleUseOperation{

    public Enum animation = null;

    @Override
    public void reset() {
        animation = null;
    }

    @Override
    public Class<? extends Executor> executorType() {
        return AnimateExecutor.class;
    }

    public static class AnimateExecutor extends SingleUseExecutor<AnimateOperation> {

        private ComponentMapper<AnimatedComponent> animatedMapper;

        @Override
        protected void act(AnimateOperation op, OperationTree node) {
            if (animatedMapper.get(op.entityId).isInitialized()) {
                animatedMapper.get(op.entityId).setCurrentAnimation(
                        op.animation.ordinal()
                );
            }

        }
    }
}
