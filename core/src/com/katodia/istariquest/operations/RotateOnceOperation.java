package com.katodia.istariquest.operations;

import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.math.Vector2;
import com.katodia.istariquest.components.ActorComponent;
import se.feomedia.orion.Executor;
import se.feomedia.orion.OperationTree;
import se.feomedia.orion.operation.SingleUseOperation;

public class RotateOnceOperation extends SingleUseOperation {

    public Vector2 direction = Vector2.Zero.cpy();

    @Override
    public void reset() {
        direction.setZero();
    }

    @Override
    public Class<? extends Executor> executorType() {
        return RotateOnceExecutor.class;
    }

    @Wire
    public static class RotateOnceExecutor extends SingleUseExecutor<RotateOnceOperation> {

        private ComponentMapper<ActorComponent> actorMapper;

        @Override
        protected void act(RotateOnceOperation operation, OperationTree node) {
            if (! operation.direction.isZero()) {
                actorMapper.get(operation.entityId).setRotation(operation.direction.angle());
            }
        }
    }
}
