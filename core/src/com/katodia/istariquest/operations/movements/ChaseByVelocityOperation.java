package com.katodia.istariquest.operations.movements;


import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.math.Vector2;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.MotionComponent;
import com.katodia.istariquest.utils.VectorUtils;
import se.feomedia.orion.Executor;
import se.feomedia.orion.OperationTree;
import se.feomedia.orion.operation.TemporalOperation;

public class ChaseByVelocityOperation extends TemporalOperation {

    public int targetId = -1;

    public boolean completed = false;

    public Vector2 vel = Vector2.Zero.cpy();

    @Override
    public Class<? extends Executor> executorType() {
        return ChaseExecutorOperation.class;
    }

    @Override
    public void reset() {
        targetId = -1;
        vel.setZero();
    }

    @Override
    public boolean isComplete() {
        return completed || super.isComplete();
    }

    @Wire
    public static class ChaseExecutorOperation
            extends TemporalExecutor<ChaseByVelocityOperation> {

        private ComponentMapper<ActorComponent> actorMapper;

        private ComponentMapper<MotionComponent> motionMapper;

        @Override
        protected void begin(ChaseByVelocityOperation op, OperationTree node) {
            super.begin(op, node);
            if (! actorMapper.has(op.targetId)) {
                // Target has died
                op.completed = true;
                return;
            }

            // Calculates duration of the movement plus vector of velocity
            Vector2 targetPosition = actorMapper.get(op.targetId).getCenteredPosition();
            Vector2 currentPosition = actorMapper.get(op.entityId).getCenteredPosition();

            Vector2 deltaPosition = VectorUtils.vectorBetween(targetPosition, currentPosition);

            float distance = targetPosition.dst(currentPosition);

            op.duration = distance / op.vel.len();

            motionMapper.get(op.entityId).setXY(
                    deltaPosition.x / op.duration, deltaPosition.y / op.duration
            );
        }

        @Override
        protected void act(float delta, float alpha, ChaseByVelocityOperation operation, OperationTree node) {

        }

        @Override
        protected void end(ChaseByVelocityOperation operation, OperationTree node) {
            motionMapper.get(operation.entityId).velocity.setZero();
        }
    }
}
