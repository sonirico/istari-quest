package com.katodia.istariquest.operations.movements;

import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.math.Vector2;
import com.katodia.istariquest.components.ActorComponent;
import se.feomedia.orion.Executor;
import se.feomedia.orion.OperationTree;
import se.feomedia.orion.operation.TemporalOperation;

public class MoveByOperation extends TemporalOperation {

    public final Vector2 src = new Vector2();
    public final Vector2 dest = new Vector2();

    @Override
    public Class<? extends Executor> executorType() {
        return MoveByExecutor.class;
    }

    @Override
    public void reset() {
        super.reset();
        src.setZero();
        dest.setZero();
    }

    @Wire
    public static class MoveByExecutor
            extends TemporalOperation.TemporalExecutor<MoveByOperation> {

        private ComponentMapper<ActorComponent> actorComponentComponentMapper;

        @Override
        protected void begin(MoveByOperation op, OperationTree node) {
            super.begin(op, node);
            op.src.set(actorComponentComponentMapper.get(op.entityId).getPosition());
            op.dest.add(op.src);
        }

        @Override
        protected void act(
            float delta,
            float percent,
            MoveByOperation op,
            OperationTree node
        ) {

            ActorComponent actorComponent = actorComponentComponentMapper.get(op.entityId);
            Vector2 pos = actorComponent.getPosition();
            pos.set(op.src).interpolate(op.dest, percent, op.interpolation);
            actorComponent.setPosition(pos);
        }
    }
}