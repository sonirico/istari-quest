package com.katodia.istariquest.operations.movements;


import com.artemis.ComponentMapper;
import com.artemis.annotations.EntityId;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.MotionComponent;
import com.katodia.istariquest.utils.VectorUtils;
import se.feomedia.orion.Executor;
import se.feomedia.orion.OperationTree;
import se.feomedia.orion.operation.TemporalOperation;

public class HarassOperation extends TemporalOperation {

    @EntityId public int targetId = -1;

    public boolean completed = false;

    public int velocity;

    @Override
    public Class<? extends Executor> executorType() {
        return HarassExecutor.class;
    }

    @Override
    public void reset() {
        super.reset();
        targetId = -1;
        completed = false;
    }

    @Override
    public boolean isComplete() {
        return completed || super.isComplete();
    }

    @Wire
    public static class HarassExecutor extends TemporalExecutor<HarassOperation>  {

        private ComponentMapper<ActorComponent> actorMapper;

        private ComponentMapper<MotionComponent> motionMapper;

        @Override
        protected void begin(HarassOperation op, OperationTree node) {
            super.begin(op, node);
        }

        @Override
        protected void act(float delta, float alpha, HarassOperation operation, OperationTree node) {
            if (! actorMapper.has(operation.targetId)) {
                // Target has died
                operation.completed = true;
                return;
            }

            Vector2 targetPos = actorMapper.get(operation.targetId).getPosition();
            Vector2 currentPos = actorMapper.get(operation.entityId).getPosition();
            Vector2 deltaSpace = VectorUtils.vectorBetween(targetPos, currentPos);

            if (deltaSpace.len() < 16) {
                motionMapper.get(operation.entityId).reset();
                operation.completed = true;
                return;
            }

            float angle = deltaSpace.angle() * MathUtils.degreesToRadians;


            motionMapper.get(operation.entityId).setXY(
                    operation.velocity * MathUtils.cos(angle),
                    operation.velocity * MathUtils.sin(angle)
            );
        }

        @Override
        protected void end(HarassOperation operation, OperationTree node) {
            motionMapper.get(operation.entityId).reset();
        }
    }
}
