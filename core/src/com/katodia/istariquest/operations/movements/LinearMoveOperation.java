package com.katodia.istariquest.operations.movements;

import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.math.Vector2;
import com.katodia.istariquest.components.MotionComponent;
import se.feomedia.orion.Executor;
import se.feomedia.orion.OperationTree;
import se.feomedia.orion.operation.TemporalOperation;

public class LinearMoveOperation extends TemporalOperation {

    public Vector2 vel = Vector2.Zero.cpy();

    @Override
    public Class<? extends Executor> executorType() {
        return LineMoveExecutor.class;
    }

    @Override
    public void reset() {
        super.reset();
        vel.setZero();
    }

    @Wire
    public static class LineMoveExecutor
            extends TemporalExecutor<LinearMoveOperation> {

        private ComponentMapper<MotionComponent> motionMapper;

        @Override
        protected void begin(LinearMoveOperation op, OperationTree node) {
            super.begin(op, node);
            motionMapper.get(op.entityId).setXY(op.vel.x / op.duration, op.vel.y / op.duration);
        }

        @Override
        protected void act(float delta, float alpha, LinearMoveOperation operation, OperationTree node) {

        }

        @Override
        protected void end(LinearMoveOperation operation, OperationTree node) {
            motionMapper.get(operation.entityId).velocity.setZero();
        }
    }
}