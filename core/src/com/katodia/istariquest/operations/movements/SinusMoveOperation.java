package com.katodia.istariquest.operations.movements;

import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.katodia.istariquest.components.MotionComponent;
import se.feomedia.orion.Executor;
import se.feomedia.orion.OperationTree;
import se.feomedia.orion.operation.TemporalOperation;

public class SinusMoveOperation extends TemporalOperation {

    public float A = 0f;
    public float phi = 0f;
    public float omega = 0f;
    public Vector2 vel = Vector2.Zero.cpy();
    public boolean xAxis = false;
    public boolean yAxis = false;
    public float t;

    @Override
    public Class<? extends Executor> executorType() {
        return SinusMoveExecutor.class;
    }

    @Override
    public void reset() {
        super.reset();
        vel.setZero();
        xAxis = false;
        yAxis = false;
        A = 0f;
        omega = 0f;
        phi = 0f;
        t = 0f;
    }

    @Wire
    public static class SinusMoveExecutor
            extends TemporalExecutor<SinusMoveOperation> {

        private ComponentMapper<MotionComponent> motionComponentComponentMapper;

        @Override
        protected void begin(SinusMoveOperation op, OperationTree node) {
            super.begin(op, node);
            op.vel.set(op.vel.x / op.duration, op.vel.y / op.duration);
        }

        @Override
        protected void act(float delta, float alpha, SinusMoveOperation op, OperationTree node) {
            if (motionComponentComponentMapper.has(op.entityId)) {
                MotionComponent mc = motionComponentComponentMapper.get(op.entityId);
                if (op.xAxis) {
                    mc.setX(getVelX(op, delta));
                } else {
                    mc.setX(op.vel.x);
                }

                if (op.yAxis) {
                    mc.setY(getVelY(op, delta));
                } else {
                    mc.setY(op.vel.y);
                }

            }
        }

        private float getVelX (SinusMoveOperation op, float delta) {
            return op.A * op.omega * MathUtils.cos(op.omega * (op.t += delta) + op.phi);
        }

        private float getVelY (SinusMoveOperation op, float delta) {
            return op.A * op.omega * MathUtils.sin(op.omega * (op.t += delta) + op.phi);
        }

        @Override
        protected void end(SinusMoveOperation operation, OperationTree node) {
            operation.vel.setZero();
            MotionComponent mc = motionComponentComponentMapper.get(operation.entityId);
            mc.velocity.set(operation.vel);
        }
    }
}