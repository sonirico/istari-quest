package com.katodia.istariquest.operations.movements;


import com.artemis.ComponentMapper;
import com.artemis.annotations.EntityId;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.math.Vector2;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.MotionComponent;
import se.feomedia.orion.Executor;
import se.feomedia.orion.OperationTree;
import se.feomedia.orion.operation.TemporalOperation;

public class ChaseByDurationOperation extends TemporalOperation {

    @EntityId public int targetEntityId = -1;

    @Override
    public Class<? extends Executor> executorType() {
        return ChaseExecutorOperation.class;
    }

    @Override
    public void reset() {
    }

    @Override
    public boolean isComplete() {
        return super.isComplete();
    }

    @Wire
    public static class ChaseExecutorOperation
            extends TemporalOperation.TemporalExecutor<ChaseByDurationOperation> {

        private ComponentMapper<ActorComponent> actorComponentComponentMapper;

        private ComponentMapper<MotionComponent> motionComponentComponentMapper;

        private Vector2 getTargetPosition (ChaseByDurationOperation op) {
            return actorComponentComponentMapper.get(op.targetEntityId).getPosition();
        }

        private Vector2 getPosition (ChaseByDurationOperation op) {
            return actorComponentComponentMapper.get(op.entityId).getPosition();
        }

        private Vector2 calculateVectorBetweenPoints (Vector2 v1, Vector2 v2) {
            Vector2 v3 = Vector2.Zero.cpy();
            v3.x = v1.x - v2.x;
            v3.y = v1.y - v2.y;
            return v3;
        }

        @Override
        protected void begin(ChaseByDurationOperation op, OperationTree node) {
            super.begin(op, node);
            Vector2 newDirection = calculateVectorBetweenPoints(getTargetPosition(op), getPosition(op));
            MotionComponent mc = motionComponentComponentMapper.get(op.entityId);
            mc.setXY(newDirection.x / op.duration, newDirection.y / op.duration);
        }

        @Override
        protected void act(float delta, float alpha, ChaseByDurationOperation operation, OperationTree node) {

        }

        @Override
        protected void end(ChaseByDurationOperation operation, OperationTree node) {
            motionComponentComponentMapper.get(operation.entityId).velocity.setZero();
        }
    }
}
