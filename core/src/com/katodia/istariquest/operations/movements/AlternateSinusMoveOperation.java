package com.katodia.istariquest.operations.movements;

import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.math.Vector2;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.MotionComponent;
import se.feomedia.orion.Executor;
import se.feomedia.orion.OperationTree;
import se.feomedia.orion.operation.TemporalOperation;

public class AlternateSinusMoveOperation extends TemporalOperation {

    public float radius = 0f;
    public Vector2 vel = Vector2.Zero.cpy();
    public boolean xAxis = false;
    public boolean yAxis = false;
    public Vector2 boundsX = Vector2.Zero.cpy();
    public Vector2 boundsY = Vector2.Zero.cpy();

    @Override
    public Class<? extends Executor> executorType() {
        return AlternateSinusMoveExecutor.class;
    }

    @Override
    public void reset() {
        super.reset();
        vel.setZero();
        radius = 0f;
        xAxis = false;
        yAxis = false;
        boundsX.setZero();
        boundsY.setZero();

    }

    @Wire
    public static class AlternateSinusMoveExecutor
            extends TemporalExecutor<AlternateSinusMoveOperation> {

        private ComponentMapper<ActorComponent> actorComponentComponentMapper;
        private ComponentMapper<MotionComponent> motionComponentComponentMapper;

        @Override
        protected void begin(AlternateSinusMoveOperation op, OperationTree node) {
            super.begin(op, node);
            ActorComponent ac = actorComponentComponentMapper.get(op.entityId);

            op.boundsX.x =  ac.getX() - op.radius;
            op.boundsX.y =  ac.getX() + op.radius;

            op.boundsY.x =  ac.getY() - op.radius;
            op.boundsY.y =  ac.getY() + op.radius;

            System.out.println(op.boundsX.toString());
            System.out.println(op.boundsY.toString());
        }

        @Override
        protected void act(float delta, float alpha, AlternateSinusMoveOperation op, OperationTree node) {
            if (motionComponentComponentMapper.has(op.entityId)) {
                MotionComponent mc = motionComponentComponentMapper.get(op.entityId);
                ActorComponent ac = actorComponentComponentMapper.get(op.entityId);

                if (op.xAxis) {
                    if (ac.getX() <= op.boundsX.x) {
                        mc.setX(op.vel.x += 5f);
                    } else if (ac.getX() >= op.boundsX.y) {
                        mc.setX(op.vel.x -= 5f);
                    } else {
                        mc.setX(op.vel.x);
                    }
                } else {
                    mc.setX(op.vel.x);
                }


                if (op.yAxis) {
                    if (ac.getY() <= op.boundsY.x) {
                        mc.setY(op.vel.y += 5f);
                    } else if (ac.getY() >= op.boundsY.y) {
                        mc.setY(op.vel.y -= 5f);
                    } else {
                        mc.setY(op.vel.y);
                    }
                } else {
                    mc.setX(op.vel.x);
                }

            }
        }

        @Override
        protected void end(AlternateSinusMoveOperation operation, OperationTree node) {
            motionComponentComponentMapper.get(operation.entityId).velocity.setZero();
        }
    }
}