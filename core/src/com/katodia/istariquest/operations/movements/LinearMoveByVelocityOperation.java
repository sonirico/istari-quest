package com.katodia.istariquest.operations.movements;

import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.math.Vector2;
import com.katodia.istariquest.components.MotionComponent;
import se.feomedia.orion.Executor;
import se.feomedia.orion.OperationTree;
import se.feomedia.orion.operation.SingleUseOperation;

public class LinearMoveByVelocityOperation extends SingleUseOperation {

    public Vector2 vel = Vector2.Zero.cpy();

    @Override
    public Class<? extends Executor> executorType() {
        return LinearMoveByVelocityExecutor.class;
    }

    @Override
    public void reset() {
        vel.setZero();
    }

    @Wire
    public static class LinearMoveByVelocityExecutor
            extends SingleUseExecutor<LinearMoveByVelocityOperation> {

        private ComponentMapper<MotionComponent> motionMapper;

        @Override
        protected void act(LinearMoveByVelocityOperation op, OperationTree node) {
            motionMapper.get(op.entityId).setXY(op.vel.x, op.vel.y);
        }
    }
}