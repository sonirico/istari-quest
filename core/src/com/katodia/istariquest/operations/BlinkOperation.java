package com.katodia.istariquest.operations;

import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.utils.Timer;
import com.katodia.istariquest.components.ActorComponent;
import se.feomedia.orion.Executor;
import se.feomedia.orion.OperationTree;
import se.feomedia.orion.operation.SingleUseOperation;

public class BlinkOperation extends SingleUseOperation {

    float minorAlpha;
    float majorAlpha;

    float delay;
    float interval;
    int repeatCount;

    @Override
    public Class<? extends Executor> executorType() {
        return BlinkExecutor.class;
    }

    @Wire
    public static class BlinkExecutor extends SingleUseExecutor<BlinkOperation> {

        private ComponentMapper<ActorComponent> actorMapper;

        @Override
        protected void act(BlinkOperation op, OperationTree node) {
            Timer.schedule(new Timer.Task() {
                boolean blink = false;
                @Override
                public void run() {
                    if (!actorMapper.has(op.entityId)) {
                        this.cancel();
                    } else {
                        blink = !blink;
                        float alpha = blink ? op.minorAlpha: op.majorAlpha;
                        actorMapper.get(op.entityId).setAlpha(alpha);
                    }
                }
            }, op.delay, op.interval, op.repeatCount);
        }
    }
}
