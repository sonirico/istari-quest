package com.katodia.istariquest.operations;

import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.katodia.istariquest.components.AnimatedComponent;
import com.katodia.istariquest.components.ArmedComponent;
import com.katodia.istariquest.components.animations.AnimationValues;
import com.katodia.istariquest.components.weapons.Weapon;
import se.feomedia.orion.Executor;
import se.feomedia.orion.OperationTree;
import se.feomedia.orion.operation.SingleUseOperation;

public class AttackOperation extends SingleUseOperation{

    public Weapon weapon;

    public final Enum defaultAnimation = AnimationValues.STATES.ATTACKING;

    public int animation = defaultAnimation.ordinal();

    @Override
    public Class<? extends Executor> executorType() {
        return AttackExecutor.class;
    }

    @Wire
    public static class AttackExecutor extends SingleUseExecutor<AttackOperation> {

        private ComponentMapper<ArmedComponent> armedComponentComponentMapper;

        private ComponentMapper<AnimatedComponent> animatedComponentComponentMapper;

        @Override
        protected void act(AttackOperation op, OperationTree node) {
            //armedComponentComponentMapper.get(op.entityId).requestAction();
            animatedComponentComponentMapper.get(op.entityId).setCurrentAnimation(
                    AnimationValues.STATES.ATTACKING.ordinal()
            );
        }
    }
}
