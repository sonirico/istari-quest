package com.katodia.istariquest.operations;


import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.ArmedComponent;
import com.katodia.istariquest.components.weapons.Weapon;
import com.katodia.istariquest.utils.EntityFactory;
import se.feomedia.orion.Executor;
import se.feomedia.orion.Operation;
import se.feomedia.orion.OperationTree;
import se.feomedia.orion.operation.SingleUseOperation;

public class ShotOperation extends SingleUseOperation {

    public Weapon weapon;

    public Operation moveDescriptor;

    public int damage = 0;

    public String [] groups;

    @Override
    public Class<? extends Executor> executorType() {
        return ShotExecutor.class;
    }

    @Wire
    public static class ShotExecutor extends SingleUseExecutor<ShotOperation> {

        private ComponentMapper<ArmedComponent> armedMapper;

        private ComponentMapper<ActorComponent> actorMapper;


        @Override
        protected void act(ShotOperation op, OperationTree node) {
            EntityFactory.shot(
                actorMapper.get(op.entityId).getBounds(),
                op.weapon,
                op.moveDescriptor,
                op.groups
            );
        }
    }
}
