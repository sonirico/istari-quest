package com.katodia.istariquest.operations;

import com.artemis.ComponentMapper;
import com.artemis.annotations.EntityId;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.math.Vector2;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.utils.VectorUtils;
import se.feomedia.orion.Executor;
import se.feomedia.orion.OperationTree;
import se.feomedia.orion.operation.TemporalOperation;

public class RotateTargetedOperation extends TemporalOperation{

	@EntityId public int targetId = -1;

	public int vel = 0;

	public boolean completed = false;

	@Override
	public boolean isComplete() {
		return completed || super.isComplete();
	}

	@Override
	public void reset() {
		super.reset();
		vel = 0;
	}

	@Override
	public Class<? extends Executor> executorType() {
		return RotateTargetedExecutor.class;
	}

	@Wire
	public static class RotateTargetedExecutor extends TemporalExecutor<RotateTargetedOperation> {

		private ComponentMapper<ActorComponent> actorMap;

		@Override
		protected void act(float delta, float alpha, RotateTargetedOperation op, OperationTree node) {
			if (! actorMap.has(op.targetId)) {
				//throw new RuntimeException("Target is not set");
				op.completed = true;
			}

			Vector2 targetPosition = actorMap.get(op.targetId).getCenteredPosition();
			Vector2 currentPosition = actorMap.get(op.entityId).getCenteredPosition();

			Vector2 deltaPosition = VectorUtils.vectorBetween(targetPosition, currentPosition);

			actorMap.get(op.entityId).setRotation(deltaPosition.angle());
		}
	}
}
