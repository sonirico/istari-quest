package com.katodia.istariquest.operations;

import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.katodia.istariquest.components.states.MeleeState;
import se.feomedia.orion.Executor;
import se.feomedia.orion.OperationTree;
import se.feomedia.orion.operation.SingleUseOperation;

public class EndMeleeOperation extends SingleUseOperation{

    @Override
    public void reset() {
        super.reset();
    }

    @Override
    public Class<? extends Executor> executorType() {
        return EndMeleeExecutor.class;
    }

    @Wire
    public static class EndMeleeExecutor extends SingleUseExecutor<EndMeleeOperation> {

        private ComponentMapper<MeleeState> mapper;

        @Override
        protected void act(EndMeleeOperation op, OperationTree node) {
            if (mapper.has(op.entityId)) {
                mapper.remove(op.entityId);
            }
        }
    }
}