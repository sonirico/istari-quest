package com.katodia.istariquest.operations;

import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.katodia.istariquest.components.ManualFlipComponent;
import se.feomedia.orion.Executor;
import se.feomedia.orion.OperationTree;
import se.feomedia.orion.operation.SingleUseOperation;

public class FlipOperation extends SingleUseOperation {

    public boolean flipX = false;

    public boolean flipY = false;

    @Override
    public void reset() {
        super.reset();
        flipX = false;
        flipY = false;
    }

    @Override
    public Class<? extends Executor> executorType() {
        return FlipExecutor.class;
    }

    @Wire
    public static class FlipExecutor extends SingleUseExecutor<FlipOperation> {

        private ComponentMapper<ManualFlipComponent> manualFlipComponentComponentMapper;

        @Override
        protected void act(FlipOperation op, OperationTree node) {
            ManualFlipComponent mfc = manualFlipComponentComponentMapper.get(op.entityId);
            mfc.flipX = op.flipX;
            mfc.flipY = op.flipY;
        }
    }
}
