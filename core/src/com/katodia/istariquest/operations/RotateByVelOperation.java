package com.katodia.istariquest.operations;

import com.artemis.ComponentMapper;
import com.artemis.annotations.Wire;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.MotionComponent;
import se.feomedia.orion.Executor;
import se.feomedia.orion.OperationTree;
import se.feomedia.orion.operation.SingleUseOperation;

public class RotateByVelOperation extends SingleUseOperation {
    @Override
    public Class<? extends Executor> executorType() {
        return RotateByVelExecutor.class;
    }

    @Wire
    public static class RotateByVelExecutor extends SingleUseExecutor<RotateByVelOperation> {

        private ComponentMapper<ActorComponent> actorMapper;
        private ComponentMapper<MotionComponent> motionMapper;

        @Override
        protected void act(RotateByVelOperation op, OperationTree node) {
            actorMapper.get(op.entityId).setRotation(
                    motionMapper.get(op.entityId).velocity.angle()
            );
        }
    }
}
