package com.katodia.istariquest.systems.states.common;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.utils.Timer;
import com.katodia.istariquest.components.AnimatedComponent;
import com.katodia.istariquest.components.animations.AnimationValues;
import com.katodia.istariquest.components.states.common.DyingState;

public class DyingStateSystem extends EntityProcessingSystem {

    private ComponentMapper<DyingState> dyingMapper;

    private ComponentMapper<AnimatedComponent> animationMapper;

    public DyingStateSystem() {
        super(Aspect.all(DyingState.class));
    }

    @Override
    protected void process(Entity e) {
        // Set dying animation
        // NOTICE that isInitialized is checked beforehand. That is needed when we create entities
        // with AnimatedC and DyingState at the same time, that means, when AnimatedC.initialized still
        // values to false. TODO: Check if working with priorities this solves. Another solution might be
        // calling AnimatedC.initialize() in these cases manually.
        if (animationMapper.has(e)) {
            AnimatedComponent animatedComponent = animationMapper.get(e);
            if (animatedComponent.hasAnimation(AnimationValues.STATES.DYING)) {
                animatedComponent.setCurrentAnimation(
                        AnimationValues.STATES.DYING.ordinal()
                );
            } else {
                animatedComponent.setDefaultAnimation();
            }
        }
        // Our time here has ended
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                getWorld().delete(e.getId());
            }
        }, dyingMapper.get(e).getTimeToFade());
        // All set
        dyingMapper.remove(e);
    }
}
