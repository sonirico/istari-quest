package com.katodia.istariquest.systems;


import com.artemis.BaseSystem;
import com.artemis.ComponentMapper;
import com.katodia.istariquest.components.DamageComponent;
import com.katodia.istariquest.components.DamageableComponent;

public class DamageSystem extends BaseSystem {

    private ComponentMapper<DamageableComponent> damageMapper;

    private ComponentMapper<DamageableComponent> damageableMapper;

    public void subtractHealth (DamageableComponent damageableC, DamageComponent damageC) {
        int damage = damageC.damage;
        int health = damageableC.getCurrentHealth();

        if (damage >= health) {
            damageableC.setCurrentHealth(-1);
        } else {
            damageableC.receivesDamage(damage);
        }
    }


    @Override
    protected void processSystem() {

    }
}
