package com.katodia.istariquest.systems;


import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.PlayableComponent;

public class LimitPlayerBoundSystem extends EntityProcessingSystem {

    private ComponentMapper<ActorComponent> actorMapper;

    public LimitPlayerBoundSystem() {
        super(Aspect.all(ActorComponent.class, PlayableComponent.class));
    }

    @Override
    protected void process(Entity entity) {
        ActorComponent actor = actorMapper.get(entity.getId());
        Rectangle bounds = actor.getBounds();

        //TODO: Softcode "10"
        if (bounds.getX() < 10) {
            actor.setX(10);
        } else if ((bounds.getX() + bounds.getWidth()) > Gdx.graphics.getWidth() - 10) {
            actor.setX(Gdx.graphics.getWidth() - bounds.getWidth() - 10);
        }

        if (bounds.getY() < 10) {
            actor.setY(10);
        } else if ((bounds.getY() + bounds.getHeight()) > Gdx.graphics.getHeight() - 10) {
            actor.setX(Gdx.graphics.getHeight() - bounds.getHeight() - 10);
        }
    }
}
