package com.katodia.istariquest.systems.collisions;


public interface CollisionObserver {
    boolean thereIsCollision(int e1, int e2);

    void handleCollision(int e1, int e2);
}
