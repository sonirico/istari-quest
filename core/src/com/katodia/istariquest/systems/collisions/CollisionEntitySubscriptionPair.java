package com.katodia.istariquest.systems.collisions;


import com.artemis.EntitySubscription;

public class CollisionEntitySubscriptionPair implements CollisionPair{

    private EntitySubscription meleeSubscription;
    private EntitySubscription damageableSubscription;

    interface CollisionHandler {
        void handleCollisions ();

        boolean hasCollision ();
    }
}
