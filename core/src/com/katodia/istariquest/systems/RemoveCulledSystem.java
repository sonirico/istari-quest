package com.katodia.istariquest.systems;


import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.IntervalEntitySystem;
import com.katodia.istariquest.components.CulledComponent;
import com.katodia.istariquest.components.LifeSpanComponent;

public class RemoveCulledSystem extends IntervalEntitySystem {

    private ComponentMapper<LifeSpanComponent> lifeSpanComponentComponentMapper;

    public RemoveCulledSystem(float interval) {
        super(Aspect.all(CulledComponent.class, LifeSpanComponent.class), interval);
    }

    @Override
    protected void processSystem() {
        for (Entity e: getEntities()) {
            LifeSpanComponent lsc = lifeSpanComponentComponentMapper.get(e);

            if (lsc.getTimer() >= lsc.getTimeToBeRemoved()) {
                getWorld().deleteEntity(e);
            } else {
                lsc.tick(getIntervalDelta());
            }
        }
    }

    public void refreshLifeSpan (Entity e) {
        // Playable entities are not supposed to disappear
        if (lifeSpanComponentComponentMapper.has(e)) {
            LifeSpanComponent lsc = lifeSpanComponentComponentMapper.get(e);
            lsc.resetTimer();
        }

    }
}
