package com.katodia.istariquest.systems;


import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.IntervalEntityProcessingSystem;
import com.artemis.utils.IntBag;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.CulledComponent;
import com.katodia.istariquest.components.MotionComponent;
import com.katodia.istariquest.components.SpawnableComponent;

public class CullingSystem extends IntervalEntityProcessingSystem{

    private ComponentMapper<ActorComponent> actorMapper;

    private ComponentMapper<CulledComponent> culledMapper;


    private Rectangle cullingArea;

    public CullingSystem(float interval) {
        super(
            Aspect.all(
                ActorComponent.class,
                MotionComponent.class
            ).exclude(SpawnableComponent.class),
            interval
        );
    }

    public CullingSystem(float interval, Rectangle rectangle) {
        this(interval);
        this.cullingArea = rectangle;
    }

    public void setCullingArea (Rectangle rectangle) {
        cullingArea.set(rectangle);
    }

    @Override
    protected void process(Entity e) {
        ActorComponent ac = actorMapper.get(e);

        boolean isCulled = culledMapper.has(e);
        boolean shouldBeCulled = ! Intersector.overlaps(ac.getBounds(), cullingArea);

        if (shouldBeCulled) {
            if (! isCulled ) {
                e.edit().create(CulledComponent.class);
                System.out.println(e.getId() + " is culled");
            }
        } else {
            if (isCulled) {
                culledMapper.remove(e);
                getWorld().getSystem(RemoveCulledSystem.class).refreshLifeSpan(e);
                System.out.println(e.getId() + " not culled anymore");
            }
        }
    }

    public int entitiesCulledCount (IntBag entityIds) {
        int total = 0;
        for (int i = 0, s = entityIds.size(); s > i; i++) {
            int entityId = entityIds.get(i);
            if (culledMapper.has(entityId)) {
                total++;
            }
        }
        return total;
    }
}
