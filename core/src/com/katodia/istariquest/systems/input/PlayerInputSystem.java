package com.katodia.istariquest.systems.input;

import com.artemis.BaseSystem;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.managers.GroupManager;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;
import com.katodia.istariquest.G;
import com.katodia.istariquest.components.PlayableComponent;


public class PlayerInputSystem implements InputProcessor {

    private IntArray players;

    private ComponentMapper<PlayableComponent> playerMapper;

    private GroupManager groupManager;

    public PlayerInputSystem() {
        players = new IntArray();
    }


    public void addPlayer (int entityId) {
		players.add(entityId);
	}

	public void removePlayer (int entityId) {
		players.removeValue(entityId);
	}

    @Override
    public boolean keyDown (int keyCode ) {
    	for (int i = 0, size = players.size; i < size; ++i) {
			playerMapper.get(players.get(i)).getVirtualBoard().put(keyCode, true);
		}

        return players.size > 0;
    }

    @Override
    public boolean keyUp (int keyCode ) {
		for (int i = 0, size = players.size; i < size; ++i) {
			playerMapper.get(players.get(i)).getVirtualBoard().put(keyCode, false);
		}

		return players.size > 0;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
