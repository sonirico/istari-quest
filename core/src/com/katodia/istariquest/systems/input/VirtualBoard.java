package com.katodia.istariquest.systems.input;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.IntMap;
import com.katodia.istariquest.components.PlayableComponent;

/**
 * Esta clase sirve para saber qué comandos se han pulsado
 */
public class VirtualBoard implements Disposable{
    // TODO: Create customizable key map


    public VirtualBoardConfiguration virtualBoardConfiguration = null;

    private IntMap<Boolean> boardMap;

    public VirtualBoard () {
        this.boardMap = new IntMap<>();

        reset();
    }


    public void setVirtualBoardConfiguration (VirtualBoardConfiguration vbc) {
        this.virtualBoardConfiguration = vbc;
    }

    public VirtualBoardConfiguration getVirtualBoardConfiguration () {
        return virtualBoardConfiguration;
    }



    /**
     * Check whether or not a command is executing
     * @param command
     * @return
     */
    public Boolean get ( Enum command ) {
        if (! virtualBoardConfiguration.containsCommand(command)) {
            return false;
        } else {
            Integer commandKey = virtualBoardConfiguration.getKeyMap().get(command.ordinal());

            if (! boardMap.containsKey(commandKey)) {
                return false;
            } else {
                return this.boardMap.get(commandKey);
            }
        }
    }

    public Boolean any () {
        for (boolean isPressed: boardMap.values()) {
            if (isPressed) {
                return true;
            }
        }

        return false;
    }

    public void put (Integer i, Boolean value) {
        this.boardMap.put(i, value);
    }

    /**
     * Initializes/sets every command value to false
     */

    public void reset() {
        for (PlayableComponent.COMMANDS command : PlayableComponent.COMMANDS.values()) {
            this.boardMap.put(command.ordinal(), false);
        }
    }

    @Override
    public void dispose() {
        this.boardMap.clear();
    }


	/**
	 * Esta clase sirve para asociar teclas con comandos
	 */

	public static class VirtualBoardConfiguration {

		public static final IntMap<Integer> defaultKeyMap;

		static {
			/*
			defaultKeyMap = new HashMap<Integer, Enum>();
			defaultKeyMap.put(Input.Keys.LEFT, VirtualBoard.COMMANDS.MOVE_LEFT);
			defaultKeyMap.put(Input.Keys.UP, VirtualBoard.COMMANDS.MOVE_UP);
			defaultKeyMap.put(Input.Keys.DOWN, VirtualBoard.COMMANDS.MOVE_DOWN);
			defaultKeyMap.put(Input.Keys.RIGHT, VirtualBoard.COMMANDS.MOVE_RIGHT);
			defaultKeyMap.put(Input.Keys.SPACE, VirtualBoard.COMMANDS.FIRE);*/
			defaultKeyMap = new IntMap<>();
			defaultKeyMap.put(PlayableComponent.COMMANDS.MOVE_LEFT.ordinal(), Input.Keys.LEFT);
			defaultKeyMap.put(PlayableComponent.COMMANDS.MOVE_UP.ordinal(), Input.Keys.UP);
			defaultKeyMap.put(PlayableComponent.COMMANDS.MOVE_DOWN.ordinal(), Input.Keys.DOWN);
			defaultKeyMap.put(PlayableComponent.COMMANDS.MOVE_RIGHT.ordinal(), Input.Keys.RIGHT);
			defaultKeyMap.put(PlayableComponent.COMMANDS.FIRE.ordinal(), Input.Keys.SPACE);
			defaultKeyMap.put(PlayableComponent.COMMANDS.UDUN_FLAME.ordinal(), Input.Keys.U);// TODO: Erase this
			defaultKeyMap.put(PlayableComponent.COMMANDS.EAGLE_CALL.ordinal(), Input.Keys.Y);// TODO: Erase this
		}

		private IntMap<Integer> keyMap;

		public VirtualBoardConfiguration () {
		}

		public void setKeyMap (IntMap<Integer> keyMap) {
			this.keyMap = keyMap;
		}

		public IntMap<Integer> getKeyMap () {
			return keyMap;
		}

		public boolean containsCommand (Enum command) {
			return keyMap.containsKey(command.ordinal());
		}
	}
}
