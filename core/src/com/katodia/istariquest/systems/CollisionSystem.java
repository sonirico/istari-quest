package com.katodia.istariquest.systems;


import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.managers.GroupManager;
import com.artemis.systems.IntervalEntitySystem;
import com.artemis.utils.Bag;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.katodia.istariquest.G;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.DamageComponent;
import com.katodia.istariquest.components.DamageableComponent;
import com.katodia.istariquest.components.InvulnerableComponent;

/**
 * @author Flet https://github.com/Flet
 *         Partially stolen from https://github.com/Flet/spaceship-warrior-gradle/blob/master/core/src/net/brokenspork/systems/CollisionSystem.java
 */

public class CollisionSystem extends IntervalEntitySystem {

    private ComponentMapper<ActorComponent> actorMapper;

    private ComponentMapper<DamageComponent> damageMapper;

    private ComponentMapper<DamageableComponent> damageableMapper;

    private ComponentMapper<InvulnerableComponent> invulnerableMapper;

    private DamageSystem damageSystem;


    private Bag<CollisionPair> collisionPairs;

    ShapeRenderer renderer;

    public CollisionSystem() {
        super(
                Aspect.all(),
                G.INGAME.CHECK_COLLISIONS_DELTA
        );
    }

    public CollisionSystem (ShapeRenderer sr) {
        this();
        this.renderer = sr;
    }

    @Override
    public void initialize() {
        collisionPairs = new Bag<CollisionPair>();

        // Bullets kill enemies

        collisionPairs.add(new CollisionPair(
            G.GROUPS.BULLET_FRIEND,
            G.GROUPS.ENEMY,
            new CollisionHandler() {
            @Override
            public void handleCollision(Entity e1, Entity e2) {
                if (invulnerableMapper.has(e2)) {
                    return;
                }
                // TODO: this gonna change
                getWorld().delete(e1.getId());
                // Subtract health
                damageSystem.subtractHealth(
                        damageableMapper.get(e2),
                        damageMapper.get(e1)
                );
            }

            @Override
            public boolean collisionExists(Entity e1, Entity e2) {
                ActorComponent e1ActorComponent = actorMapper.get(e1);
                ActorComponent e2ActorComponent = actorMapper.get(e2);

                Rectangle bulletRectangle = e1ActorComponent.getBounds();
                Rectangle objectRectangle = e2ActorComponent.getBounds();

                return Intersector.overlaps(bulletRectangle, objectRectangle);
            }
        }
        ));

        // Bullets collide with the world

        collisionPairs.add(new CollisionPair(
                G.GROUPS.BULLET,
                G.GROUPS.COLLIDABLE_ELEMENT,
                new CollisionHandler() {
                    @Override
                    public void handleCollision(Entity e1, Entity e2) {
                        getWorld().delete(e1.getId());
                    }

                    @Override
                    public boolean collisionExists(Entity e1, Entity e2) {
                        ActorComponent e1ActorComponent = actorMapper.get(e1);
                        ActorComponent e2ActorComponent = actorMapper.get(e2);

                        Rectangle bulletRectangle = e1ActorComponent.getBounds();
                        Rectangle objectRectangle = e2ActorComponent.getBounds();

                        return Intersector.overlaps(bulletRectangle, objectRectangle);
                    }
                }
        ));

        // Dynamic entities vs world static elements

        collisionPairs.add(new CollisionPair(
                G.GROUPS.COLLIDABLE_ACTOR,
                G.GROUPS.COLLIDABLE_ELEMENT,
                new CollisionHandler() {

                    @Override
                    public void handleCollision(Entity e1, Entity e2) {
                        // For better performance, handling is done in collisionExists
                        return;
                    }

                    @Override
                    public boolean collisionExists(Entity e1, Entity e2) {
                        ActorComponent e1ActorComponent = actorMapper.get(e1);
                        ActorComponent e2ActorComponent = actorMapper.get(e2);

                        Rectangle entityRectangle = e1ActorComponent.getBounds();
                        Rectangle objectRectangle = e2ActorComponent.getBounds();

                        Rectangle intersection = new Rectangle();

                        if  ( Intersector.intersectRectangles(entityRectangle, objectRectangle, intersection) ) {
                            // The logic beneath is as improvable as unreadable if improved
                            boolean xAxis = false, yAxis = false;
                            if (intersection.width < intersection.height ) {
                                // We can assume we were moving over the X axis
                                xAxis = true;
                            } else if ( intersection.height < intersection.width ) {
                                // We can assume we were moving over the Y axis
                                yAxis = true;
                            } else {
                                // Have we hit a corner while traveling in 45 degrees?
                                xAxis = yAxis = true;
                            }

                            if (xAxis)
                            if(intersection.x  > entityRectangle.x) {
                                // Intersects with right side
                                e1ActorComponent.setX(objectRectangle.x - entityRectangle.width);
                            } else if(intersection.x  < objectRectangle.x + objectRectangle.width) {
                                // Intersects with left side
                                e1ActorComponent.setX(objectRectangle.x + objectRectangle.width);
                            }

                            if (yAxis)
                            if(intersection.y > entityRectangle.y){
                                // Intersects with top side
                                e1ActorComponent.setY(objectRectangle.y - entityRectangle.height);
                            } else if(intersection.y < objectRectangle.y + objectRectangle.height){
                                // Intersects with bottom side
                                e1ActorComponent.setY(objectRectangle.y + objectRectangle.height);
                            }

                            if (G.DEBUG) {
                                renderer.begin(ShapeRenderer.ShapeType.Filled);
                                renderer.setColor(Color.PINK);
                                renderer.rect(intersection.getX(), intersection.getY(), intersection.getWidth(), intersection.height);
                                renderer.end();
                            }
                            return true;
                        } else { return false; }
                    }
                }
        ));
    }

    @Override
    protected void processSystem() {
        for (CollisionPair collisionPair : collisionPairs) {
            collisionPair.checkForCollisions();
        }
    }

    private class CollisionPair {

        private ImmutableBag<Entity> groupEntitiesA;
        private ImmutableBag<Entity> groupEntitiesB;
        private CollisionHandler collisionHandler;

        public CollisionPair(String group1, String group2, CollisionHandler handler) {
            groupEntitiesA = world.getSystem(GroupManager.class).getEntities(group1);
            groupEntitiesB = world.getSystem(GroupManager.class).getEntities(group2);
            collisionHandler = handler;
        }


        public void checkForCollisions() {
            for (Entity entityA : groupEntitiesA) {
                for (Entity entityB : groupEntitiesB) {
                    if (collisionHandler.collisionExists(entityA, entityB)) {
                        collisionHandler.handleCollision(entityA, entityB);
                    }
                }
            }
        }


    } // End of CollisionPair.class

    private interface CollisionHandler {

        void handleCollision(Entity e1, Entity e2);

        boolean collisionExists(Entity e1, Entity e2);

    }
}
