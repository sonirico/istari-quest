package com.katodia.istariquest.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.AnimatedComponent;
import com.katodia.istariquest.components.SpawnableComponent;

public class AnimationSystem extends IteratingSystem {

    private ComponentMapper<AnimatedComponent> animatedComponentComponentMapper;
    private ComponentMapper<ActorComponent> actorComponentComponentMapper;

    private float deltaTime = 0f;

    public AnimationSystem () {
        super(
            Aspect.all(
                AnimatedComponent.class,
                ActorComponent.class
            ).exclude(
                SpawnableComponent.class
            )
        );
    }

    @Override
    public void begin () {
        deltaTime = getWorld().getDelta();
    }


    @Override
    protected void process(int entityId) {
        AnimatedComponent animatedComponent = animatedComponentComponentMapper.get(entityId);

        if (! animatedComponent.isInitialized()) {
            animatedComponent.initialize();
        }

        ActorComponent actorComponent = actorComponentComponentMapper.get(entityId);
        actorComponent.setTextureRegion(animatedComponent.getKeyFrame());

        animatedComponent.tick(deltaTime);
    }

    public void setAnimationForEntity ( Enum animationId , int entityId ) {
        AnimatedComponent animatedComponent = animatedComponentComponentMapper.get(entityId);
        animatedComponent.setCurrentAnimation(animationId.ordinal());
        animatedComponent.resetTimer();
    }

    public void flip (int entityId, boolean x, boolean y) {
        animatedComponentComponentMapper.get(entityId).flip(x, y);
    }
}
