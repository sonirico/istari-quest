package com.katodia.istariquest.systems;


import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.PooledComponent;
import com.artemis.World;
import com.artemis.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.ObjectMap;

import java.util.HashMap;
import java.util.Map;

public class ScriptSystem extends IteratingSystem {

    private ComponentMapper<ScriptComponent> scriptMap;

    public ScriptSystem () {
        super(Aspect.all(ScriptComponent.class));
    }

    public void current (int entityId, Class<? extends Script> scriptClass) {
        ScriptComponent sc = scriptMap.get(entityId);

        // Cleanup operations
        sc.getCurrentScript().end(entityId);

        Script current = sc.getScripts().get(scriptClass);
        sc.setCurrentScript(current);

		// Initializing operations
        current.begin(entityId);
    }


	@Override
	protected void process(int entityId) {
		ScriptComponent sc = scriptMap.get(entityId);
		// Check for fresh added scripts
		sc.getScripts().values().forEach( script -> {
			if (! script.initialized) {
				script.inject(world);
			}
		});

		sc.process(entityId);
	}


	public static class ScriptComponent extends PooledComponent {

		private ObjectMap<Class<? extends Script>, Script> scripts;

		private Script currentScript;

		public ScriptComponent () {
			scripts = new ObjectMap<>();
		}

		public ScriptComponent add (Class<? extends Script> scriptClass) {
			try {
				this.scripts.put(scriptClass, scriptClass.newInstance());
			} catch (Exception e) {
				Gdx.app.log(e.getCause().toString(), e.getMessage());
			} finally {
				return this;
			}
		}

		public void current (Class<? extends Script> scriptClass) {

			this.currentScript = scripts.get(scriptClass);

		}

		public void setCurrentScript (Script s) {
			this.currentScript = s;
		}

		public Script getCurrentScript () {
			return this.currentScript;
		}

		public ObjectMap<Class<? extends Script>, Script> getScripts () {
			return this.scripts;
		}

		public void process (int entityId) {
			this.currentScript.process(entityId);
		}

		@Override
		protected void reset() {
			scripts.clear();
		}
	}


	public static abstract class Script {

		protected World world;

		protected boolean initialized = false;

		public Script inject (World world) {
			if (initialized) {
				if (this.world.equals(world)) {
					throw new RuntimeException("World is already injected");
				} else {
					return this;
				}
			}

			this.world = world;
			this.initialized = true;

			return this;
		}

		public abstract void begin (int entityId);

		public abstract void process (int entityId);

		public abstract void end (int entityId);
	}
}
