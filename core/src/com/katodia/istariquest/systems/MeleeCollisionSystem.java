package com.katodia.istariquest.systems;

import com.artemis.*;
import com.artemis.utils.IntBag;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.katodia.istariquest.G;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.DamageableComponent;
import com.katodia.istariquest.components.InvulnerableComponent;
import com.katodia.istariquest.components.states.MeleeState;
import com.katodia.istariquest.systems.collisions.CollisionObserver;

public class MeleeCollisionSystem extends BaseSystem implements CollisionObserver {

    private EntitySubscription meleeSubscription;
    private EntitySubscription damageableSubscription;

    private ComponentMapper<MeleeState> meleeMapper;
    private ComponentMapper<DamageableComponent> damageableMapper;
    private ComponentMapper<ActorComponent> actorMapper;

    private ShapeRenderer renderer;

    public MeleeCollisionSystem() {
        if (G.DEBUG) {
            renderer = new ShapeRenderer();
        }
    }

    @Override
    protected void initialize() {
        AspectSubscriptionManager asm = getWorld().getAspectSubscriptionManager();
        meleeSubscription = asm.get(
                Aspect.all(ActorComponent.class, MeleeState.class)
        );
        damageableSubscription = asm.get(
                Aspect.all(ActorComponent.class, DamageableComponent.class).exclude(InvulnerableComponent.class)
        );
    }

    @Override
    protected void processSystem() {
        IntBag meleeIds = meleeSubscription.getEntities();
        IntBag damageableIds = damageableSubscription.getEntities();

        for (int i = 0; i < meleeIds.size(); ++i) {
            for (int j = 0; j < damageableIds.size() ; ++j) {
                int e1 = meleeIds.get(i);
                int e2 = damageableIds.get(j);
                if (e1 == e2) continue;
                if (thereIsCollision(e1, e2)) {
                    handleCollision(e1, e2);
                }
            }
        }
    }


    @Override
    public boolean thereIsCollision(int e1, int e2) {
        ActorComponent e1ActorComponent = actorMapper.get(e1);
        ActorComponent e2ActorComponent = actorMapper.get(e2);

        Rectangle e1Bounds = e1ActorComponent.getBounds();
        Rectangle e2Bounds = e2ActorComponent.getBounds();

        // Some debug featuring
        if (G.DEBUG && e1Bounds.overlaps(e2Bounds)) {
            renderer.begin(ShapeRenderer.ShapeType.Filled);

            renderer.setColor(Color.ORANGE);

            renderer.rect(e1Bounds.getX(), e1Bounds.getY(), e1Bounds.getWidth(), e1Bounds.getHeight());

            renderer.setColor(Color.YELLOW);

            renderer.rect(e2Bounds.getX(), e2Bounds.getY(), e2Bounds.getWidth(), e2Bounds.getHeight());

            renderer.end();
        }

        return e1Bounds.overlaps(e2Bounds);
    }

    @Override
    public void handleCollision(int e1, int e2) {

        int damage = meleeMapper.get(e1).getDamage();
        int health = damageableMapper.get(e2).getCurrentHealth();

        if (damage >= health) {
            damageableMapper.get(e2).setCurrentHealth(-1);
        } else {
            Gdx.app.log("entity " + e1, " receives " + damage + " damage");
            damageableMapper.get(e2).receivesDamage(damage);
        }
    }
}
