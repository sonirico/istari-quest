package com.katodia.istariquest.systems;


import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.managers.GroupManager;
import com.artemis.systems.EntityProcessingSystem;
import com.katodia.istariquest.G;
import com.katodia.istariquest.components.*;
import com.katodia.istariquest.components.animations.AnimationE;
import com.katodia.istariquest.components.states.common.DyingState;
import se.feomedia.orion.system.OperationSystem;

import static com.katodia.istariquest.operations.CustomOperationFactory.blink;

public class DeathSystem extends EntityProcessingSystem {

    private ComponentMapper<DamageableComponent> damageableMapper;

    private ComponentMapper<ActorComponent> actorMapper;

    private ComponentMapper<MotionComponent> motionMapper;

    private ComponentMapper<OperationComponent> operationMapper;

    private ScoreSystem scoreSystem;

    private GroupManager groupManager;

    private OperationSystem operationSystem;

    public DeathSystem() {
        super(Aspect.all(DamageableComponent.class).exclude(PlayableComponent.class));
    }

    @Override
    protected void process(Entity e) {
        if (damageableMapper.get(e.getId()).getCurrentHealth() < 1) {
            // Remove pending operations
            if (operationMapper.has(e)) {
                operationSystem.clear(e.getId());
                operationMapper.remove(e);
            }
			// Stop moving
			motionMapper.remove(e);
            // The enemy can not longer be hit
            e.edit().create(InvulnerableComponent.class);
            // Make the enemy blink
            e.edit().create(OperationComponent.class).setOperation(
                    blink(.2f, .8f, 0, .1f, 10)
            );
            // CollectPoints
            scoreSystem.collectScore(e.getId(), G.ENEMY_TYPES.SOLE_ENEMY);
            // Common ground enemies
            if (groupManager.isInGroup(e, G.SIZES.NORMAL)) {
				// Set dying state
				e.edit().create(DyingState.class).setTimeToFade(1);
                //Spawn little flame TODO: Move it to entity factory
                Entity flame = getWorld().createEntity();
                flame.edit().create(ActorComponent.class).setBounds(
                        actorMapper.get(e.getId()).getBounds()
                );

                flame.edit().create(AnimatedComponent.class).setAnimation(AnimationE.DYING_FLAME_NORMAL).initialize();
                flame.edit().create(DyingState.class).setTimeToFade(1);
                flame.edit().create(OperationComponent.class).setOperation(
                        blink(.2f, .8f, 0, .1f, 10)
                );
            } // else if (...) {} else if (...) {}
            // No further processing required
            damageableMapper.remove(e);
        } // End process
    }
}
