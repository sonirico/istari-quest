package com.katodia.istariquest.systems.render;


import com.artemis.BaseSystem;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.katodia.istariquest.systems.ScoreSystem;

public class RenderScoreSystem extends BaseSystem {

    private SpriteBatch batch;

    private BitmapFont font;

    private ScoreSystem ss;

    public RenderScoreSystem setBatch(SpriteBatch batch) {
        this.batch = batch;

        return this;
    }

    public RenderScoreSystem setFont(BitmapFont font) {
        this.font = font;

        return this;
    }

    @Override
    protected void begin() {
        batch.begin();
    }

    @Override
    protected void processSystem() {
        font.draw(batch, "SCORE: " + ss.getTotalScore(), 20, 20);
    }

    @Override
    protected void end() {
        batch.end();
    }
}
