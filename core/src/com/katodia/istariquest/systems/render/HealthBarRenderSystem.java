package com.katodia.istariquest.systems.render;


import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.DamageableComponent;

public class HealthBarRenderSystem extends EntityProcessingSystem {

    private ShapeRenderer shapeRenderer;

    private ComponentMapper<ActorComponent> actorMapper;

    private ComponentMapper<DamageableComponent> healthMapper;

    public HealthBarRenderSystem () {
        super(Aspect.all(ActorComponent.class, DamageableComponent.class));
    }

    public HealthBarRenderSystem (ShapeRenderer sr) {
        this();
        this.shapeRenderer = sr;
    }

    @Override
    protected void begin() {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
    }

    @Override
    protected void process(Entity e) {
        ActorComponent ac = actorMapper.get(e.getId());
        DamageableComponent dc = healthMapper.get(e.getId());

        float percent = ((dc.getCurrentHealth() * 1f) / dc.getTotalHealth());

        if (percent >= 1f) {
            shapeRenderer.setColor(0, 1, 0, .5f);
            shapeRenderer.rect(ac.getX(), ac.getY() + ac.getHeight() + 8, ac.getWidth(), 8);
        } else {
            shapeRenderer.setColor(0, 1, 0, .5f);
            shapeRenderer.rect(ac.getX(), ac.getY() + ac.getHeight() + 8, ac.getWidth() * percent, 8);

            shapeRenderer.setColor(0, 0, 0, 1);
            shapeRenderer.rect(
                    ac.getX() + ac.getWidth() * percent,
                    ac.getY() + ac.getHeight() + 8,
                    ac.getWidth() * (1f - percent),
                    8
            );
        }


    }

    @Override
    protected void end() {
        shapeRenderer.end();
    }
}
