package com.katodia.istariquest.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.ArmedComponent;
import com.katodia.istariquest.components.CulledComponent;
import com.katodia.istariquest.utils.EntityFactory;

public class WeaponSystem extends IteratingSystem {

    private ComponentMapper<ArmedComponent> armedComponentComponentMapper;
    private ComponentMapper<ActorComponent> actorComponentComponentMapper;

    private float deltaTime = 0f;

    public WeaponSystem () {
        super(
            Aspect.all(
                ArmedComponent.class,
                ActorComponent.class
            ).exclude(
                CulledComponent.class
            )
        );
    }

    @Override
    public void begin () {
        deltaTime = getWorld().getDelta();
    }


    @Override
    protected void process(int entityId) {
        ArmedComponent armedComponent = armedComponentComponentMapper.get(entityId);
        if (armedComponent.canTrigger()) {
            if (armedComponent.isActionRequested()) {
                ActorComponent actorComponent = actorComponentComponentMapper.get(entityId);
                EntityFactory.buildWeapon(
                        getWorld(),
                        actorComponent,
                        armedComponent
                );

                armedComponent.resetTimer();
            }
        } else {
            armedComponent.tick(deltaTime);
        }
    }

    public void requestFire (int entityId) {
        ArmedComponent armedComponent = armedComponentComponentMapper.get(entityId);
        armedComponent.requestAction();
    }

    public void cancelRequest (int entityId) {
        ArmedComponent armedComponent = armedComponentComponentMapper.get(entityId);
        armedComponent.clearActionRequest();
    }
}
