package com.katodia.istariquest.systems;


import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.utils.Bag;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.AnimatedComponent;
import com.katodia.istariquest.components.CulledComponent;
import com.katodia.istariquest.components.SpawnableComponent;
import com.katodia.istariquest.utils.SortingUtils;

public class RenderSystem extends EntitySystem {

    private SpriteBatch spriteBatch;

    private ComponentMapper<ActorComponent> actorMapper;

    private float timer = 0f;

    public RenderSystem () {
        super(
            Aspect.all(
                ActorComponent.class,
                AnimatedComponent.class
            ).exclude(
                SpawnableComponent.class,
                CulledComponent.class
            )
        );
    }

    public RenderSystem setSpriteBatch (SpriteBatch batch) {
        this.spriteBatch = batch;

        return this;
    }

    @Override
    protected void begin() {
        spriteBatch.begin();
        timer += getWorld().getDelta();
    }

    @Override
    protected void processSystem() {
		Bag<Entity> entityBag = getEntities();
        // Order entities by y axis to get a feeling of depth
        if (timer > .05f) {
			entityBag = SortingUtils.sortByAxis(getEntities());
        }
        // Finally they are draw
        for (Entity e : entityBag) {
            actorMapper.get(e.getId()).draw(this.spriteBatch);
        }
    }


    @Override
    protected void end() {
        spriteBatch.end();
    }
}
