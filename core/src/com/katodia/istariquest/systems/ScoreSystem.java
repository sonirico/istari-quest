package com.katodia.istariquest.systems;

import com.artemis.BaseSystem;
import com.artemis.ComponentMapper;
import com.katodia.istariquest.G;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.ScoreComponent;

public class ScoreSystem extends BaseSystem {

    private int totalScore = 0;

    private ComponentMapper<ScoreComponent> pointMapper;

    private ComponentMapper<ActorComponent> actorMapper;


    public int getTotalScore() {
        return this.totalScore;
    }

    @Override
    protected void processSystem() {
        // Process hordes
    }

    public void collectScore(int entityId, Enum enemyTag) {
        if (pointMapper.has(entityId)) {
            int points = pointMapper.get(entityId).getPoints();
            int enemyTagID = enemyTag.ordinal();
            this.totalScore += points;

            if (enemyTagID == G.ENEMY_TYPES.SOLE_ENEMY.ordinal()) {

            } else if (enemyTagID == G.ENEMY_TYPES.BOSS.ordinal()) {

            } else if (enemyTagID == G.ENEMY_TYPES.HORDE.ordinal()) {
                // place number in otoher place
            }
        }
    }
}
