package com.katodia.istariquest.systems;

import com.artemis.Aspect;
import com.artemis.BaseSystem;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.IntervalEntityProcessingSystem;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.katodia.istariquest.G;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.RectangleCollidableComponent;

public class ScrollSystem extends IntervalEntityProcessingSystem{

    private ComponentMapper<ActorComponent> actorComponentComponentMapper;

    private Vector2 direction;

    private OrthographicCamera tileMapCamera;

    private int distanceTravelled = 0;

    public ScrollSystem() {
        super(
            Aspect.all(RectangleCollidableComponent.class, ActorComponent.class),
            G.INGAME.SCROLL_DELTA
        );
        direction = new Vector2(0, 0);
    }

    public ScrollSystem(OrthographicCamera camera) {
        this();
        this.tileMapCamera = camera;
    }

    @Override
    public void begin () {
        int deltaSpace = G.INGAME.SCROLL_DISTANCE;
        this.distanceTravelled += deltaSpace;
        tileMapCamera.translate(deltaSpace * direction.x * -1, deltaSpace * direction.y * -1);
        tileMapCamera.update();
    }

    @Override
    protected void process(Entity e) {
        ActorComponent actorComponent = actorComponentComponentMapper.get(e);
        actorComponent.moveBy(
            G.INGAME.SCROLL_DISTANCE * direction.x,
            G.INGAME.SCROLL_DISTANCE * direction.y
        );
    }

    public BaseSystem setDirection (Vector2 dir) {
        if (null == dir) return this;

        if (null != this.direction){
            direction.x = dir.x;
            direction.y = dir.y;
        } else {
            this.direction = dir;
        }

        return this;
    }

    public int getDistanceTravelled () {
        return this.distanceTravelled;
    }
}
