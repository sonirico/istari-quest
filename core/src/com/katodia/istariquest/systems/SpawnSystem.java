package com.katodia.istariquest.systems;


import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;
import com.katodia.istariquest.components.SpawnableComponent;


public class SpawnSystem extends IteratingSystem {

    private ComponentMapper<SpawnableComponent> spawnableComponentComponentMapper;

    private int currentDistance = 0;

    public SpawnSystem () {
        super(
            Aspect.all(SpawnableComponent.class)
        );
    }

    @Override
    public void begin () {
        currentDistance = getWorld().getSystem(ScrollSystem.class).getDistanceTravelled();
    }

    @Override
    protected void process(int entityId) {
        SpawnableComponent spawnableComponent = spawnableComponentComponentMapper.get(entityId);

        if (spawnableComponent.canSpawn(currentDistance)) {
            spawnableComponentComponentMapper.remove(entityId);
        }
    }
}
