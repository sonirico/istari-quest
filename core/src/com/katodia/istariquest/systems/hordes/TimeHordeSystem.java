package com.katodia.istariquest.systems.hordes;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IntervalIteratingSystem;
import com.katodia.istariquest.components.CulledComponent;
import com.katodia.istariquest.components.hordes.TimeHordeComponent;

public class TimeHordeSystem extends IntervalIteratingSystem {

    private ComponentMapper<TimeHordeComponent> hordeMapper;

    protected ComponentMapper<CulledComponent> culledMapper;

    public TimeHordeSystem() {
        super(Aspect.all(TimeHordeComponent.class), .1f);
    }

    @Override
    protected void process(int entityId) {
        TimeHordeComponent hc = hordeMapper.get(entityId);

        if (hc.isEmpty()) {
            // Full score, TODO: Link with point system
            hordeMapper.get(entityId).getPoints();
            // The end
            getWorld().delete(entityId);
        }

        hc.tick(getWorld().getDelta());
    }
}
