package com.katodia.istariquest.systems.hordes;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IntervalIteratingSystem;
import com.katodia.istariquest.G;
import com.katodia.istariquest.components.CulledComponent;
import com.katodia.istariquest.components.hordes.HordeComponent;
import com.katodia.istariquest.systems.ScoreSystem;

public class HordeSystem extends IntervalIteratingSystem {

    private ComponentMapper<HordeComponent> hordeMapper;

    protected ComponentMapper<CulledComponent> culledMapper;

    public HordeSystem() {
        super(Aspect.all(HordeComponent.class), .1f);
    }

    @Override
    protected void process(int entityId) {
        HordeComponent hc = hordeMapper.get(entityId);
        int totalSize = hc.getTotalSize();
        int eCulled = 0;
        boolean processed = false;

        if (hc.isEmpty()) {
            // Full score
            // Collect score
            getWorld().getSystem(ScoreSystem.class).collectScore(entityId, G.ENEMY_TYPES.HORDE);
            processed = true;
        }

        /*else if ((eCulled = getWorld.getSystem(Cullingsystem.class).entitiesCulled(hc.entityIds)) == totalSize) {
            // We haven't killed anyone :(
            processed = true;
        } else {
            // Distribute spare score accordingly
            int percent = 100 * eCulled / totalSize;
            if (percent >= 90) {

            } else if (percent >= 75) {

            } else if (percent >= 50) {

            } else {
                processed = false;
            }
        }
*/
        // Processed
        if (processed) {
            getWorld().delete(entityId);
        }
    }
}
