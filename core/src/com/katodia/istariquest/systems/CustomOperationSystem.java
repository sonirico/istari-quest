package com.katodia.istariquest.systems;


import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;
import com.katodia.istariquest.components.OperationComponent;

public class CustomOperationSystem extends EntityProcessingSystem {

    private ComponentMapper<OperationComponent> mapper;

    public CustomOperationSystem() {
        super(Aspect.all(OperationComponent.class));
    }

    @Override
    protected void process(Entity e) {
        OperationComponent oc = mapper.get(e);

        if (!oc.registered) {
            oc.register(e);
            oc.registered = true;
        }
    }
}
