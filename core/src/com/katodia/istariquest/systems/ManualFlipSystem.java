package com.katodia.istariquest.systems;


import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;
import com.katodia.istariquest.components.AnimatedComponent;
import com.katodia.istariquest.components.ManualFlipComponent;

public class ManualFlipSystem extends EntityProcessingSystem {

    private ComponentMapper<ManualFlipComponent> manualFlipComponentComponentMapper;

    private ComponentMapper<AnimatedComponent> animatedComponentComponentMapper;

    public ManualFlipSystem() {
        super(Aspect.all(
            ManualFlipComponent.class,
            AnimatedComponent.class
        ));
    }

    @Override
    protected void process(Entity entity) {
        ManualFlipComponent mfc = manualFlipComponentComponentMapper.get(entity.getId());
        boolean x = false, y = false;

        if (mfc.flipX) {
            x = true;
            mfc.flipX = false;
        }

        if (mfc.flipY) {
            y = true;
            mfc.flipY = false;
        }

        if (x || y) {
            getWorld().getSystem(AnimationSystem.class).flip(entity.getId(), x, y);
        }
    }
}
