package com.katodia.istariquest.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.MotionComponent;
import com.katodia.istariquest.components.PlayableComponent;
import com.katodia.istariquest.components.SpawnableComponent;

public class MovementSystem extends IteratingSystem {

    private ComponentMapper<ActorComponent> actorComponentComponentMapper;
    private ComponentMapper<MotionComponent> motionComponentComponentMapper;

    private float deltaTime;

    public MovementSystem () {
        super(
            Aspect.all(
                ActorComponent.class,
                MotionComponent.class
            ).exclude(PlayableComponent.class, SpawnableComponent.class)
        );
    }

    @Override
    public void begin () {
        deltaTime = this.getWorld().getDelta();
    }

    @Override
    protected void process(int entityId) {
        ActorComponent actorComponent = actorComponentComponentMapper.get(entityId);
        MotionComponent v = motionComponentComponentMapper.get(entityId);

        actorComponent.moveBy(v.velocity.x * deltaTime, v.velocity.y * deltaTime);
    }

    public void move (int entityId, int dirX, int dirY) {
        ActorComponent actorComponent = actorComponentComponentMapper.get(entityId);
        MotionComponent v = motionComponentComponentMapper.get(entityId);
        actorComponent.moveBy(
            Math.abs(v.velocity.x) * deltaTime * dirX,
            Math.abs(v.velocity.y) * deltaTime * dirY
        );
    }

    public void move (int entityId, float deltaX, float deltaY) {
        ActorComponent actorComponent = actorComponentComponentMapper.get(entityId);
        actorComponent.moveBy(deltaX, deltaY);
    }
}
