package com.katodia.istariquest.systems;


import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.math.Vector2;
import com.katodia.istariquest.components.AnimatedComponent;
import com.katodia.istariquest.components.MotionComponent;
import com.katodia.istariquest.components.MotionFlipComponent;

public class MotionFlipSystem extends EntityProcessingSystem {

    private ComponentMapper<MotionFlipComponent> flipComponentComponentMapper;

    private ComponentMapper<AnimatedComponent> animatedComponentComponentMapper;

    private ComponentMapper<MotionComponent> motionComponentComponentMapper;

    public MotionFlipSystem() {
        super(Aspect.all(
            MotionFlipComponent.class,
            MotionComponent.class
        ));
    }

    @Override
    protected void process(Entity entity) {
        MotionComponent mc = motionComponentComponentMapper.get(entity.getId());
        MotionFlipComponent mfc = flipComponentComponentMapper.get(entity.getId());

        // Initialization is needed because of the non-zero comparison on hasChangedDirection* methods
        if (mfc.lastVel.isZero()) {
            mfc.lastVel.set(mc.velocity);
        }

        boolean flipX = mfc.flipX && hasChangedDirectionX(mc.velocity, mfc.lastVel);
        boolean flipY = mfc.flipY && hasChangedDirectionY(mc.velocity, mfc.lastVel);

        if (flipX || flipY) {
            mfc.lastVel.set(mc.velocity);
            getWorld().getSystem(AnimationSystem.class).flip(entity.getId(), flipX, flipY);
        }
    }

    public boolean hasChangedDirectionX (Vector2 currentVel, Vector2 lastVel) {
        return (currentVel.x > 0 && lastVel.x < 0) || (currentVel.x < 0 && lastVel.x > 0);
    }

    public boolean hasChangedDirectionY (Vector2 currentVel, Vector2 lastVel) {
        return (currentVel.y > 0 && lastVel.y < 0) || (currentVel.y < 0 && lastVel.y > 0);
    }
}
