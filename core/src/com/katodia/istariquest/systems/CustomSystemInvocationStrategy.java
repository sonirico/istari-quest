package com.katodia.istariquest.systems;


import com.artemis.BaseSystem;
import com.artemis.SystemInvocationStrategy;
import com.artemis.utils.Bag;
import com.badlogic.gdx.utils.Array;
import com.katodia.istariquest.systems.ifs.HighPrioritySystem;
import com.katodia.istariquest.systems.ifs.RenderSystemIF;

import java.util.concurrent.TimeUnit;

public class CustomSystemInvocationStrategy extends SystemInvocationStrategy{

    private final Array<BaseSystem> logicHighPrioritySystems;
    private final Array<BaseSystem> logicNormalPrioritySystems;
    private final Array<BaseSystem> renderSystems;

    private boolean sorted;

    private long accumulator = 0L;

    private long currentTime = System.nanoTime();

    private long nanosPerLogicTick;

    public CustomSystemInvocationStrategy () {
        this(40);
    }

    private CustomSystemInvocationStrategy (int millisPerLogicTick) {
        this.nanosPerLogicTick = TimeUnit.MILLISECONDS.toNanos(millisPerLogicTick);
        logicHighPrioritySystems = new Array<BaseSystem>();
        logicNormalPrioritySystems = new Array<BaseSystem>();
        renderSystems = new Array<BaseSystem>();
    }

    private void sortSystems (Bag<BaseSystem> systems) {
        if (! sorted) {
            Object[] systemsData = systems.getData();
            for (int i = 0, s = systems.size(); s > i; i++) {
                BaseSystem system = (BaseSystem) systemsData[i];

                if (system instanceof HighPrioritySystem) {
                    logicHighPrioritySystems.add(system);
                } else if (system instanceof RenderSystemIF) {
                    renderSystems.add(system);
                } else {
                    logicNormalPrioritySystems.add(system);
                }
            }
            sorted = true;
        }
    }


    @Override
    protected void process() {
        if (false == sorted) {
            sortSystems(this.systems);
        }

        long newTime = System.nanoTime();
        long frameTime = newTime - currentTime;

        if (frameTime > 250000000) {
            frameTime = 250000000;    // Note: Avoid spiral of death
        }

        currentTime = newTime;
        accumulator += frameTime;

        /**
         * Uncomment this line if you use the world's delta within your systems.
         * I recommend to use a fixed value for your logic delta like millisPerLogicTick or nanosPerLogicTick
         */
//		world.setDelta(nanosPerLogicTick * 0.000000001f);

        while (accumulator >= nanosPerLogicTick) {
            /** Process all entity systems inheriting from {@link LogicRenderMarker} */
            for (BaseSystem system : logicHighPrioritySystems) {
                system.process();
                updateEntityStates();
            }

            for (BaseSystem system : logicNormalPrioritySystems) {
                /**
                 * Make sure your systems keep the current state before calculating the new state
                 * else you cannot interpolate later on when rendering
                 */
                system.process();
                updateEntityStates();
            }

            accumulator -= nanosPerLogicTick;
        }

        for (BaseSystem system : renderSystems) {
            /**
             * Make sure your systems keep the current state before calculating the new state
             * else you cannot interpolate later on when rendering
             */
            system.process();
            updateEntityStates();
        }
    }
}
