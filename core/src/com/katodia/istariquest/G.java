package com.katodia.istariquest;


public class G {

    public static final String gameMode = "development"; // Realease

    public static final boolean DEBUG = true;

    public static class INGAME {
        public static final int SCROLL_DISTANCE = 16;

        public static final float SCROLL_DELTA = 1f;

        public static final float CHECK_COLLISIONS_DELTA = .01f;
    }

    public static class UI {

        public static final int WIDTH = 480;

        public static final int HEIGHT = 320;

        public static final String TITLE = "Otro juego que no me hará rico";

        public static final boolean RESIZABLE = true;

        public static final boolean FULL_SCREEN = true;
    }

    public static class GROUPS {

        public static final String PLAYER = "players";
        public static final String COLLIDABLE_ACTOR = "collidable-actor";
        public static final String COLLIDABLE_ELEMENT = "collidable-element";
        public static final String BULLET = "bullet";
        public static final String BULLET_FRIEND = "bullet-friend";
        public static final String ENEMY = "enemy";

    }

    public static class TAGS {

        public static final String PLAYER = "player";

    }

    public static class SIZES {

        public static final String SMALL = "small";

        public static final String NORMAL = "normal"; // 32x32

        public static final String BIG = "big";

    }

    public enum ENEMY_TYPES {
        SOLE_ENEMY,
        HORDE,
        BOSS
    };
}
