package com.katodia.istariquest;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.katodia.istariquest.screens.MainMenuScreen;


public class IstariQuest extends Game {
	public SpriteBatch batch;
	public BitmapFont font;

    public Viewport viewport;

	@Override
	public void create () {
		batch = new SpriteBatch();
		font = new BitmapFont();
        viewport = new FitViewport(G.UI.WIDTH, G.UI.HEIGHT);

        AssetsHolder.load();

		this.setScreen(new MainMenuScreen(this));
		//this.setScreen(new GameScreen(this));
	}

	@Override
	public void render () {
		super.render();
		batch.begin();
		font.draw(batch, "fps:"+ Gdx.graphics.getFramesPerSecond(), 26, 65);
		batch.end();
	}

	public void dispose () {
		batch.dispose();
		font.dispose();
        AssetsHolder.dispose();
	}


    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        viewport.update(width, height);
        viewport.apply();
    }
}
