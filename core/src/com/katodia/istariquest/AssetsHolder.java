package com.katodia.istariquest;


import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;

public final class AssetsHolder {
    
    private static AssetManager assetManager;

    private AssetsHolder () {}

    private static void init () {
        assetManager = new AssetManager();
    }
    
    public static void load () {

        if (null == assetManager) init();

        assetManager.load("player.png", Texture.class);
        assetManager.load("Standard Blast.png", Texture.class);
        assetManager.load("White Saruman.png", Texture.class);
        assetManager.load("Gandalf The Grey.png", Texture.class);
        assetManager.load("Gandalf The Grey 2.png", Texture.class);
        assetManager.load("Gandalf The Grey 3.png", Texture.class);
        assetManager.load("Gandalf The White.png", Texture.class);
        assetManager.load("Dun Ragadast.png", Texture.class);
        assetManager.load("Palantir.png", Texture.class);
        assetManager.load("malo_1.png", Texture.class);

        assetManager.load("Orc Melee 1.png", Texture.class);
        assetManager.load("Orc Crossbow 1.png", Texture.class);
        assetManager.load("skeleton normal.png", Texture.class);
        assetManager.load("skeleton archer.png", Texture.class);
        assetManager.load("skeleton melee.png", Texture.class);
        assetManager.load("Bat 1.png", Texture.class);
        assetManager.load("Spider.png", Texture.class);
        assetManager.load("Spider 2.png", Texture.class);
        assetManager.load("Ent.png", Texture.class);

        assetManager.load("bullets/Magic Missile.png", Texture.class);
        assetManager.load("bullets/Elf Arrow.png", Texture.class);
        assetManager.load("bullets/Udun Flame.png", Texture.class);
        assetManager.load("bullets/Udun Flame 3.png", Texture.class);
        assetManager.load("bullets/Spinning Flame.png", Texture.class);

        assetManager.load("Dying Flame Normal.png", Texture.class);
        assetManager.load("frame.png", Texture.class);

        assetManager.setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));
        assetManager.load("maps/stage1.tmx", TiledMap.class);
        assetManager.load("maps/new_stage1.tmx", TiledMap.class);

        // REMOVE if a progress bar is wanted
        // Check AssetManager.update and AssetManager.getProgress in that case
        assetManager.finishLoading();// Assures every single resource is already loaded
    }

    public static <T> T get (String key) {
        return assetManager.get(key);
    }

    public static <T> T get (String key, Class<T> type) {
        return assetManager.get(key, type);
    }

    public static void dispose() {
        assetManager.dispose();
    }
}
