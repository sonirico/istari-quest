package com.katodia.istariquest.utils;


import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.katodia.istariquest.G;
import com.katodia.istariquest.components.*;
import com.katodia.istariquest.components.animations.AnimationE;
import com.katodia.istariquest.components.weapons.Shot;
import com.katodia.istariquest.components.weapons.Weapon;
import com.katodia.istariquest.scripts.playable.*;
import com.katodia.istariquest.systems.MovementSystem;
import com.katodia.istariquest.systems.ScriptSystem.ScriptComponent;
import com.katodia.istariquest.systems.input.VirtualBoard;
import se.feomedia.orion.Operation;
import se.feomedia.orion.system.OperationSystem;

import java.util.ArrayList;
import java.util.List;


public final class EntityFactory {

	private static EntityFactory instance = new EntityFactory();

	private World world;

	private EntityFactory () {

	}

	public static void setWorld (final World world) {
		instance.world = world;
	}

	public static Entity shot (Rectangle bounds, Weapon weapon, Operation operation, String [] groups) {

        final OperationSystem os = instance.world.getSystem(OperationSystem.class);

        for (Shot shot: weapon.getBurst()) {

            Entity bullet = instance.world.createEntity();

            bullet.edit().create(ActorComponent.class).setBounds(new Rectangle(
                    bounds.getX() + shot.getSpawnPoint().x,
                    bounds.getY() + shot.getSpawnPoint().y,
                    weapon.getWidth(), weapon.getHeight()
            ));

            bullet.edit().create(MotionComponent.class);
            bullet.edit().create(DamageComponent.class).setDamage(1);
            bullet.edit().create(AnimatedComponent.class).setAnimation(weapon.getAnimation());
            bullet.edit().create(OperationComponent.class).setOperation(os.copy(operation));
            bullet.edit().create(LifeSpanComponent.class).setTimeToBeRemoved(10);

            for (String group: groups) {
                instance.world.getSystem(GroupManager.class).add(bullet, group);
            }
        }

        return null;
	}

	public static List<Entity> buildWeapon (World world, ActorComponent triggerer, ArmedComponent ac) {
		List<Entity> bullets = new ArrayList<Entity>();

		for (Shot shot : ac.getCurrentWeapon().getBurst()) {

			Entity weapon = instance.world.createEntity();

			ActorComponent actorComponent = weapon.edit().create(ActorComponent.class);

			actorComponent.setBounds(
				new Rectangle(
					triggerer.getX() + shot.getSpawnPoint().x,
					triggerer.getY() + shot.getSpawnPoint().y,
					ac.getCurrentWeapon().getWidth(),
					ac.getCurrentWeapon().getHeight()
				)
			);

			MotionComponent motionComponent = weapon.edit().create(MotionComponent.class);
			motionComponent.setXY(shot.getDirection().x, shot.getDirection().y);

			AnimatedComponent animatedComponent = weapon.edit().create(AnimatedComponent.class);

			animatedComponent.setAnimation(ac.getCurrentWeapon().getAnimation());

			CollidableComponent collidableComponent = weapon.edit().create(CollidableComponent.class);
			DamageComponent damageComponent = weapon.edit().create(DamageComponent.class);
			damageComponent.damage = 5;

			LifeSpanComponent lsc = weapon.edit().create(LifeSpanComponent.class);
			lsc.setTimeToBeRemoved(1);

			for (String tag : ac.getCurrentWeapon().getGroups()) {
				world.getSystem(GroupManager.class).add(weapon, tag);
			}

			bullets.add(weapon);
		}

		return bullets;
	}

	public static Entity spawnPlayer (World world, VirtualBoard board, MapProperties mps) {
		Entity player = world.createEntity();

		player.edit().create(ActorComponent.class).setBounds(new Rectangle(
                mps.get("x", Float.class),
                mps.get("y", Float.class),
                32, 32
        ));

		player.edit().create(AnimatedComponent.class).setAnimation(AnimationE.GANDALF);
		player.edit().create(PlayableComponent.class).setVirtualBoard(board);
        player.edit().create(MotionComponent.class).setXY(100, 100);
        player.edit().create(ArmedComponent.class).setCurrentWeapon(Weapon.WIDE_SHOT);
        player.edit().create(DamageableComponent.class).setHealth(50);
        player.edit().create(ScriptComponent.class)
                .add(WalkScript.class)
                .add(AttackScript.class)
                .add(UdunScript.class)
                .add(EagleCallScript.class)
                .add(SmokeScript.class)
                .current(WalkScript.class);

		world.getSystem(GroupManager.class).add(player, G.GROUPS.COLLIDABLE_ACTOR);
		world.getSystem(GroupManager.class).add(player, G.GROUPS.PLAYER);
		world.getSystem(TagManager.class).register(G.TAGS.PLAYER, player);
        
		return player;
	}

	public static void spawnUdunFlames (Rectangle bounds, int nFlames) {
        GroupManager groupManager = instance.world.getSystem(GroupManager.class);
        MovementSystem movementSystem = instance.world.getSystem(MovementSystem.class);

        final float angle = 360f / nFlames;
        for (float phi = 0; phi <= 360; phi += angle) {
            Entity flame = instance.world.createEntity();

            flame.edit().create(ActorComponent.class).setBounds(bounds);
            flame.edit().create(AnimatedComponent.class).setAnimation(AnimationE.SPINNING_FLAME);
            flame.edit().create(MotionComponent.class).setXY(
                    MathUtils.random(100, 150) * MathUtils.cos(phi * MathUtils.degreesToRadians),
                    MathUtils.random(100, 150) * MathUtils.sin(phi * MathUtils.degreesToRadians)
            );
            flame.edit().create(LifeSpanComponent.class).setTimeToBeRemoved(3);
            // Spawn them just a little away from the player
            movementSystem.move(
                    flame.getId(),
                    bounds.getWidth() * MathUtils.cos(phi * MathUtils.degreesToRadians),
                    bounds.getHeight() * MathUtils.sin(phi * MathUtils.degreesToRadians)
            );

            groupManager.add(flame, G.GROUPS.BULLET_FRIEND);
        }
    }

    public static void spawnUdunFlamesVertically (Rectangle bounds) {
        final float overlapping = 1.7f;
        int nFlames = (int) (Gdx.graphics.getHeight() / bounds.getHeight() * overlapping);

        for (int i = 0; i <= nFlames * 2; i++) {
            Entity flame = instance.world.createEntity();

            flame.edit().create(ActorComponent.class).setBounds(new Rectangle(
                    bounds.getX(),
                    (i % nFlames) * bounds.getHeight() / overlapping,
                    bounds.getWidth(), bounds.getHeight()
            ));
            AnimatedComponent ac = flame.edit().create(AnimatedComponent.class);
            ac.setAnimation(AnimationE.UDUN_FLAME_3);
            ac.startAtRandomFrame = true;

            int dir = 1;
            if (i >= nFlames) {
                dir = -1;
                ac.flipX = true;
            }

            flame.edit().create(MotionComponent.class).setXY(dir * 100, 0);
            flame.edit().create(LifeSpanComponent.class).setTimeToBeRemoved(3);

            instance.world.getSystem(GroupManager.class).add(flame, G.GROUPS.BULLET_FRIEND);
        }
    }
}
