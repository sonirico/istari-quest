package com.katodia.istariquest.utils;


import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.katodia.istariquest.G;
import com.katodia.istariquest.components.*;
import com.katodia.istariquest.components.animations.AnimationE;
import com.katodia.istariquest.components.animations.AnimationValues;
import com.katodia.istariquest.components.hordes.HordeComponent;
import com.katodia.istariquest.components.weapons.Weapon;
import se.feomedia.orion.Operation;

import java.util.Stack;

import static com.katodia.istariquest.operations.CustomOperationFactory.*;
import static se.feomedia.orion.OperationFactory.*;

public class StageBuilder {

    private final World world;

    public Stack<Stage> gameStage;

    private final Entity player;

    public StageBuilder (World world) {
        this.gameStage = new Stack<Stage>();
        this.world = world;

        player = world.getSystem(TagManager.class).getEntity(G.TAGS.PLAYER);
    }

    public void setUpStageOne() {
        gameStage.add(0, new Stage());

        // First horde
        Entity horde_1 = world.createEntity();
        // Morcego

        Entity morcego = world.createEntity();

        morcego.edit().create(EnemyComponent.class);

        ActorComponent ac1 = morcego.edit().create(ActorComponent.class);
        ac1.setBounds(new Rectangle(300, 120, 32, 32));

        AnimatedComponent acc1 = morcego.edit().create(AnimatedComponent.class);
        acc1.setAnimation(AnimationE.BAT);

        morcego.edit().create(MotionComponent.class);

        SpawnableComponent sc1 = morcego.edit().create(SpawnableComponent.class);
        sc1.setDistanceToAppear(32);

        DamageableComponent dcMorcego = morcego.edit().create(DamageableComponent.class);
        dcMorcego.setHealth(10);

        Operation o = forever(
                sequence(
                        sinusMove(50, 2, 90, seconds(4), xy(-200, 0), false, true),
                        chaseByVelocity(
                                player.getId(),
                                xy(100, 100)
                        )
                )

        );

        OperationComponent oc = morcego.edit().create(OperationComponent.class);
        oc.setOperation(o);


        LifeSpanComponent lsc1 = morcego.edit().create(LifeSpanComponent.class);
        lsc1.setTimeToBeRemoved(3);

        // Orc

        Entity orc = world.createEntity();

        orc.edit().create(EnemyComponent.class);
        orc.edit().create(ActorComponent.class).setBounds(new Rectangle(400, 120, 32, 32));
        orc.edit().create(AnimatedComponent.class).setAnimation(AnimationE.ORC_CROSSBOW_1);
        orc.edit().create(MotionComponent.class);
        orc.edit().create(DamageableComponent.class).setHealth(20);
        orc.edit().create(SpawnableComponent.class).setDistanceToAppear(0);
        orc.edit().create(MotionFlipComponent.class).flipX = true;

        Operation o2 = forever(sequence(
                animate(AnimationValues.STATES.WALKING),
                moveBy(new Vector2(-50, 0), seconds(2)),
                animate(AnimationValues.STATES.ATTACKING),
                delay(seconds(1)),
                shot(
                        Weapon.ELF_ARROW,
                        sequence(
                                move(new Vector2(-100, 50)),
                                rotateOnce(new Vector2(-2, 1))
                        ),
                        G.GROUPS.BULLET
                ),
                shot(
                        Weapon.ELF_ARROW,
                        sequence(
                                targetedShot(player.getId(), 200),
                                rotateByVel()
                        ),
                        G.GROUPS.BULLET
                ),
                shot(
                        Weapon.ELF_ARROW,
                        sequence(
                                move(new Vector2(-100, -50)),
                                rotateOnce(new Vector2(-2, -1))
                        ),
                        G.GROUPS.BULLET
                )

        ));

        orc.edit().create(OperationComponent.class).setOperation(o2);
        orc.edit().create(MotionComponent.class);
        orc.edit().create(LifeSpanComponent.class).setTimeToBeRemoved(3);
        orc.edit().create(ScoreComponent.class).setPoints(10);

        world.getSystem(GroupManager.class).add(orc, G.GROUPS.ENEMY);
        world.getSystem(GroupManager.class).add(orc, G.GROUPS.COLLIDABLE_ACTOR);
        world.getSystem(GroupManager.class).add(orc, G.SIZES.NORMAL);

        Entity orc_melee_1 = orc_melee_1();

        System.out.println("Horde: " + horde_1.toString());
        System.out.println("Orc melee: " + orc_melee_1.toString());
        System.out.println("Orc archer" + orc.toString());

        horde_1.edit().create(HordeComponent.class).with(orc_melee_1).with(orc).march();
        horde_1.edit().create(ScoreComponent.class).setPoints(23);

        for (int i = 0; i < 20; ++i) {

			System.out.println("Ent" + skeleton(AnimationE.ENT));
		}
        //skeleton(AnimationE.SKELETON_NORMAL);
        //skeleton(AnimationE.SKELETON_ARCHER);
    }

    private Entity orc_melee_1 () {
        Entity orc = world.createEntity();

        //orc.edit().create(ScriptComponent.class).add(new EmptyScript().inject(world));
        orc.edit().create(EnemyComponent.class);
        orc.edit().create(ScoreComponent.class).setPoints(12);
        orc.edit().create(ActorComponent.class).setBounds(new Rectangle(G.UI.WIDTH, 220, 32, 32));
        orc.edit().create(AnimatedComponent.class).setAnimation(AnimationE.ORC_MELEE_1);
        orc.edit().create(MotionComponent.class);
        orc.edit().create(DamageableComponent.class).setHealth(20);
        orc.edit().create(SpawnableComponent.class).setDistanceToAppear(0);
        orc.edit().create(MotionFlipComponent.class).flipX = true;

        Operation o2 = forever(sequence(
                animate(AnimationValues.STATES.WALKING),
                //moveBy(xy(-100, 0), 1),
                //chaseByVelocity(player.getId(), new Vector2(100, 100)),
                harass(player.getId(), 60, 5),
                animate(AnimationValues.STATES.ATTACKING),
                melee(4),
                delay(seconds(1)),
                endMelee()
        ));

        orc.edit().create(OperationComponent.class).setOperation(o2);
        orc.edit().create(LifeSpanComponent.class).setTimeToBeRemoved(3);

        world.getSystem(GroupManager.class).add(orc, G.GROUPS.ENEMY);
        world.getSystem(GroupManager.class).add(orc, G.GROUPS.COLLIDABLE_ACTOR);
        world.getSystem(GroupManager.class).add(orc, G.SIZES.NORMAL);

        return orc;
    }

    private Entity skeleton (AnimationE animation) {
        System.out.println("Actual width: " + Gdx.graphics.getWidth());
        Entity orc = world.createEntity();

        int x = (int)(Math.random() * 100) + G.UI.WIDTH;
		int y = (int)(Math.random() * 200) + 64;

        orc.edit().create(EnemyComponent.class);
        orc.edit().create(ScoreComponent.class).setPoints(12);
		orc.edit().create(ActorComponent.class).setBounds(new Rectangle(x, y, 48, 48));
        //orc.edit().create(ActorComponent.class).setBounds(new Rectangle(G.UI.WIDTH, 120, 48, 48));
        orc.edit().create(AnimatedComponent.class).setAnimation(animation);
        orc.edit().create(MotionComponent.class);
        orc.edit().create(DamageableComponent.class).setHealth(20);
        orc.edit().create(SpawnableComponent.class).setDistanceToAppear(10);
        orc.edit().create(MotionFlipComponent.class).flipX = true;

        Operation o2 = forever(sequence(
                animate(AnimationValues.STATES.WALKING),
                //moveBy(xy(-100, 0), 1),
                //chaseByVelocity(player.getId(), new Vector2(100, 100)),
                //harass(player.getId(), 30, 5),
				move(new Vector2(-30, 0)),
                animate(AnimationValues.STATES.ATTACKING),
                delay(seconds(1))
        ));

        orc.edit().create(OperationComponent.class).setOperation(o2);
        orc.edit().create(LifeSpanComponent.class).setTimeToBeRemoved(3);

        world.getSystem(GroupManager.class).add(orc, G.GROUPS.ENEMY);
        world.getSystem(GroupManager.class).add(orc, G.GROUPS.COLLIDABLE_ACTOR);
        world.getSystem(GroupManager.class).add(orc, G.SIZES.NORMAL);

        return orc;
    }

    private class Stage {
        int ID;
    }
}
