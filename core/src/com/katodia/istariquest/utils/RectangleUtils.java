package com.katodia.istariquest.utils;


import com.badlogic.gdx.math.Rectangle;

public final class RectangleUtils {

    private RectangleUtils () {}

    public Rectangle greater (Rectangle r1, Rectangle r2) {
        if (r1.area() > r2.area()) return r1;
        else return r2;
    }

    public Rectangle smaller (Rectangle r1, Rectangle r2) {
        if (r1.area() < r2.area()) return r1;
        else return r2;
    }
}
