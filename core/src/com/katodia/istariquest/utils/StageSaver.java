package com.katodia.istariquest.utils;


import com.artemis.Aspect;
import com.artemis.AspectSubscriptionManager;
import com.artemis.EntitySubscription;
import com.artemis.World;
import com.artemis.io.SaveFileFormat;
import com.artemis.managers.WorldSerializationManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.katodia.istariquest.components.EnemyComponent;

public final class StageSaver {


    public static void saveStage (World world, int stageId) {
        //world.process();
        final EntitySubscription enemies = world.getSystem(AspectSubscriptionManager.class).get(
                Aspect.all(EnemyComponent.class)
        );

        try {
            FileHandle handle = Gdx.files.local(String.format("levels/%s.json", stageId));
            final SaveFileFormat sff = new SaveFileFormat(enemies.getEntities());
            world.getSystem(WorldSerializationManager.class).save(
                    handle.write(false), sff
            );
        } catch (Exception e) {
            // TODO: Why does this happen?
            Gdx.app.error("MyGame", "Save Failed", e);
        }
    }
}
