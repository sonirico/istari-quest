package com.katodia.istariquest.utils;


import com.artemis.World;
import com.artemis.io.SaveFileFormat;
import com.artemis.managers.WorldSerializationManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.GdxRuntimeException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class StageLoader {


    public static void load (World world, int stage) {
        if (stage <= 0) {
            throw new IllegalArgumentException("Stage number must be greater than 0");
        }
        String stageFile = String.format("levels/%s.json", stage);
        Gdx.app.log("FILE", "loading stagefile " + stageFile);

        try {
            FileHandle fh = Gdx.files.internal(stageFile);
            String content = fh.readString();

            ByteArrayInputStream is = null;
            try {
                is = new ByteArrayInputStream(content.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            SaveFileFormat sff = world.getSystem(WorldSerializationManager.class).load(
                    is,
                    SaveFileFormat.class
            );

            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //sff.entities
            System.out.println("Entities loaded: " + sff.entities.size());
        } catch (GdxRuntimeException gre) {
            Gdx.app.log("MESSAGE", gre.getMessage());
            Gdx.app.log("NO Entities loaded", "0");
        }

    }
}
