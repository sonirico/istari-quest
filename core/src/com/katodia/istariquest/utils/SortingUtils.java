package com.katodia.istariquest.utils;


import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.utils.Bag;
import com.katodia.istariquest.components.ActorComponent;

public final class SortingUtils {

	private static ComponentMapper<ActorComponent> actorMapper;

	public static Bag<Entity> sortByAxis (Bag<Entity> unsortedEntities) {
		unsortedEntities.sort((e1, e2) -> {
			float a1y = actorMapper.get(e1).getY();
			float a2y = actorMapper.get(e2).getY();

			if (a1y > a2y) return -1;
			else if (a1y < a2y) return 1;
			else  return 0;
		});

		return unsortedEntities;
	}
}
