package com.katodia.istariquest.utils;


import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public final class VectorUtils {

    private VectorUtils () {}

    /**
     * Returns the vector between two points
     * @param v1
     * @param v2
     * @return
     */

    public static Vector2 vectorBetween (Vector2 v1, Vector2 v2) {
        Vector2 v3 = Vector2.Zero.cpy();
        v3.x = v1.x - v2.x;
        v3.y = v1.y - v2.y;
        return v3;
    }

    /**
     * Returns the center point of the rectangle
     * @param bounds
     * @return
     */

    public static Vector2 centeredPosition (Rectangle bounds) {
        return new Vector2(bounds.x + bounds.getWidth() / 2, bounds.y + bounds.getHeight()/ 2);
    }
}
