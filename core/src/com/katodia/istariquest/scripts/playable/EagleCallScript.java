package com.katodia.istariquest.scripts.playable;

import com.artemis.Entity;
import com.badlogic.gdx.utils.Timer;
import com.katodia.istariquest.components.InvulnerableComponent;
import com.katodia.istariquest.components.animations.AnimationValues;
import com.katodia.istariquest.scripts.EmptyScript;
import com.katodia.istariquest.systems.AnimationSystem;
import com.katodia.istariquest.systems.ScriptSystem;

public class EagleCallScript extends EmptyScript {

    public static float DURATION = 1;

    @Override
    public void process(int entityId) {
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                //EntityFactory.spawnEagles();
                world.getMapper(InvulnerableComponent.class).remove(entityId);

                world.edit(entityId).create(InvulnerableComponent.class);
                world.getSystem(ScriptSystem.class).current(entityId, WalkScript.class);

                world.getSystem(AnimationSystem.class).setAnimationForEntity(
                        AnimationValues.STATES.WALKING,
                        entityId
                );
            }
        }, EagleCallScript.DURATION);

    }
}
