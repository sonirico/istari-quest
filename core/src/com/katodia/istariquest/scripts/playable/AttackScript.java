package com.katodia.istariquest.scripts.playable;

import com.artemis.Entity;
import com.katodia.istariquest.components.PlayableComponent;
import com.katodia.istariquest.components.animations.AnimationValues;
import com.katodia.istariquest.scripts.EmptyScript;
import com.katodia.istariquest.systems.AnimationSystem;
import com.katodia.istariquest.systems.MovementSystem;
import com.katodia.istariquest.systems.ScriptSystem;
import com.katodia.istariquest.systems.WeaponSystem;
import com.katodia.istariquest.systems.input.VirtualBoard;

public class AttackScript extends EmptyScript {

    @Override
    public void process(int entityId) {
        PlayableComponent pc = world.getMapper(PlayableComponent.class).get(entityId);

        VirtualBoard vb = pc.getVirtualBoard();

        if (vb.get(PlayableComponent.COMMANDS.FIRE)) {
            world.getSystem(WeaponSystem.class).requestFire(entityId);
        } else {
            world.getSystem(AnimationSystem.class).setAnimationForEntity(
                    AnimationValues.STATES.WALKING,
					entityId
            );
            world.getSystem(WeaponSystem.class).cancelRequest(entityId);
            world.getSystem(ScriptSystem.class).current(entityId, WalkScript.class);
        }

        // We can keep moving
        int dirX = 0, dirY = 0;

        if (vb.get(PlayableComponent.COMMANDS.MOVE_LEFT) != vb.get(PlayableComponent.COMMANDS.MOVE_RIGHT)){
            dirX = vb.get(PlayableComponent.COMMANDS.MOVE_LEFT) ? -1 : 1;
        }

        if (vb.get(PlayableComponent.COMMANDS.MOVE_UP) != vb.get(PlayableComponent.COMMANDS.MOVE_DOWN)){
            dirY = vb.get(PlayableComponent.COMMANDS.MOVE_UP) ? 1 : -1;
        }

        world.getSystem(MovementSystem.class).move(entityId, dirX, dirY);
    }

}
