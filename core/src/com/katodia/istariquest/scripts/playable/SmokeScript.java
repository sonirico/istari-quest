package com.katodia.istariquest.scripts.playable;


import com.artemis.Entity;
import com.katodia.istariquest.components.PlayableComponent;
import com.katodia.istariquest.components.animations.AnimationValues;
import com.katodia.istariquest.scripts.EmptyScript;
import com.katodia.istariquest.systems.AnimationSystem;
import com.katodia.istariquest.systems.ScriptSystem;

public class SmokeScript extends EmptyScript{

    public SmokeScript() {

    }

    @Override
    public void process(int entityId) {
        if (world.getMapper(PlayableComponent.class).get(entityId).getVirtualBoard().any()) {
            world.getSystem(ScriptSystem.class).current(entityId, WalkScript.class);
            world.getSystem(AnimationSystem.class).setAnimationForEntity(
                    AnimationValues.STATES.WALKING,
					entityId
            );
        }
    }
}
