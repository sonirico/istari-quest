package com.katodia.istariquest.scripts.playable;


import com.artemis.Entity;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Timer;
import com.katodia.istariquest.components.ActorComponent;
import com.katodia.istariquest.components.AnimatedComponent;
import com.katodia.istariquest.components.InvulnerableComponent;
import com.katodia.istariquest.components.animations.AnimationValues;
import com.katodia.istariquest.scripts.EmptyScript;
import com.katodia.istariquest.systems.AnimationSystem;
import com.katodia.istariquest.systems.ScriptSystem;
import com.katodia.istariquest.utils.EntityFactory;

public class UdunScript extends EmptyScript {
    
    public static final float SPELL_TIME_LENGTH = 1f;

    public static final int nFlames = 24;

    private boolean isInitialized = false;

    public UdunScript() {}

    @Override
    public void begin(int entityId) {
        isInitialized = false;
    }

    @Override
    public void process(int entityId) {
        final Rectangle bounds = world.getMapper(ActorComponent.class).get(entityId).getBounds();

        if (! isInitialized) {
            isInitialized = true;

            // Spawn flames after the spell
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    EntityFactory.spawnUdunFlames(bounds, nFlames);
                }
            }, SPELL_TIME_LENGTH);

            // Back to WalkState
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    world.getMapper(InvulnerableComponent.class).remove(entityId);
                    world.getSystem(ScriptSystem.class).current(entityId, WalkScript.class);
                    world.getSystem(AnimationSystem.class).setAnimationForEntity(
                            AnimationValues.STATES.WALKING,
							entityId
                    );
                }
            }, world.getMapper(AnimatedComponent.class).get(entityId).getAnimationDuration());
        }
    }

    @Override
    public void end(int entityId) {
        isInitialized = false;
    }
}
