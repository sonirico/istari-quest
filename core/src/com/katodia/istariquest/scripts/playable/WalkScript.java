package com.katodia.istariquest.scripts.playable;

import com.artemis.Entity;
import com.katodia.istariquest.components.InvulnerableComponent;
import com.katodia.istariquest.components.PlayableComponent;
import com.katodia.istariquest.components.animations.AnimationValues;
import com.katodia.istariquest.scripts.EmptyScript;
import com.katodia.istariquest.systems.AnimationSystem;
import com.katodia.istariquest.systems.MovementSystem;
import com.katodia.istariquest.systems.ScriptSystem;
import com.katodia.istariquest.systems.ScriptSystem.ScriptComponent;
import com.katodia.istariquest.systems.input.VirtualBoard;


public class WalkScript extends EmptyScript {

    private float timer = 0f;

    private static final int timeToRest = 5;

    private boolean mustRest () {
        return timer >= timeToRest;
    }

    private void tick (float dt) {
        timer += dt;
    }

    private void resetTimer() {
        timer = 0f;
    }

    @Override
    public void process(int entityId) {
        PlayableComponent pc = world.getMapper(PlayableComponent.class).get(entityId);
        ScriptComponent sc = world.getMapper(ScriptComponent.class).get(entityId);

        Integer dirX = 0;
        Integer dirY = 0;

        VirtualBoard vb = pc.getVirtualBoard();

        if (vb.get(PlayableComponent.COMMANDS.MOVE_LEFT)
                != vb.get(PlayableComponent.COMMANDS.MOVE_RIGHT)){
            dirX = vb.get(PlayableComponent.COMMANDS.MOVE_LEFT) ? -1 : 1;
            resetTimer();
        }

        if (vb.get(PlayableComponent.COMMANDS.MOVE_UP) != vb.get(PlayableComponent.COMMANDS.MOVE_DOWN)){
            dirY = vb.get(PlayableComponent.COMMANDS.MOVE_UP) ? 1 : -1;
            resetTimer();
        }

        world.getSystem(MovementSystem.class).move(entityId, dirX, dirY);

        if (vb.get(PlayableComponent.COMMANDS.FIRE)) {
            world.getSystem(AnimationSystem.class).setAnimationForEntity(
                    AnimationValues.STATES.ATTACKING,
					entityId
            );

            resetTimer();
            sc.current(AttackScript.class);
        }

        if ( mustRest() ) {
            world.getSystem(ScriptSystem.class).current(entityId, SmokeScript.class);
            world.getSystem(AnimationSystem.class).setAnimationForEntity(
                    AnimationValues.STATES.SMOKING,
					entityId
            );

            resetTimer();
        } else {
            tick(world.getDelta());
        }

        if (vb.get(PlayableComponent.COMMANDS.UDUN_FLAME)) {
            world.getSystem(ScriptSystem.class).current(entityId, UdunScript.class);
            world.edit(entityId).create(InvulnerableComponent.class);
            world.getSystem(AnimationSystem.class).setAnimationForEntity(
                    AnimationValues.STATES.UDUN_FLAME,
					entityId
            );

            resetTimer();
        }

        if (vb.get(PlayableComponent.COMMANDS.EAGLE_CALL)) {
            world.getSystem(ScriptSystem.class).current(entityId, EagleCallScript.class);
			world.edit(entityId).create(InvulnerableComponent.class);
            world.getSystem(AnimationSystem.class).setAnimationForEntity(
                    AnimationValues.STATES.SPEAKING_BUTTERFLY,
                    entityId
            );

            resetTimer();
        }
    }
}
