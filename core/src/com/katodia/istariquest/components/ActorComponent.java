package com.katodia.istariquest.components;


import com.artemis.PooledComponent;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.katodia.istariquest.G;

public class ActorComponent extends PooledComponent{

    private Actor actor;

    private TextureRegion textureRegion;

    private Vector2 position;

    private float alpha = 1;


    public ActorComponent () {
        actor = new Actor();
        position = new Vector2();
        //Gdx.app.log("Actor", "Spawn - " + this.toString());
        actor.setDebug(G.DEBUG);
    }

    public ActorComponent (float x, float y) {
        this();
        actor.setX(x);
        actor.setY(y);
        actor.setWidth(0);
        actor.setHeight(0);
    }


    public ActorComponent (float x, float y, float w, float h) {
        this(x, y);
        actor.setWidth(w);
        actor.setHeight(h);
    }

    public void act (float dt) {
        actor.act(dt);
    }

    public void addAction (Action action) {
        actor.addAction(action);
    }

    public void addListener (EventListener listener) {
        actor.addListener(listener);
    }

    public void setX (float x) {
        actor.setX(x);
        position.x = x;
    }

    public void setY (float y) {
        actor.setY(y);
        position.y = y;
    }

    public void setWidth (float w) {
        actor.setWidth(w);
    }

    public void setHeight (float h) {
        actor.setHeight(h);
    }

    public float getX () {
        return actor.getX();
    }

    public float getY () {
        return actor.getY();
    }

    public float getWidth () {
        return actor.getWidth();
    }

    public float getHeight () {
        return actor.getHeight();
    }

    public float getRotation () {
        return actor.getRotation();
    }

    public float getOriginX () {
        return actor.getOriginX();
    }

    public float getOriginY () {
        return actor.getOriginY();
    }

    public float getScaleX () {
        return actor.getScaleX();
    }

    public float getScaleY () {
        return actor.getScaleY();
    }

    public TextureRegion getTextureRegion () {
        return textureRegion;
    }

    public void setTextureRegion (TextureRegion tr) {
        this.textureRegion = tr;
    }

    public void setVisible (boolean visible) {
        actor.setVisible(visible);
    }

    public boolean isVisible () {
        return actor.isVisible();
    }

    public void moveBy (float dx, float dy) {
        actor.moveBy(dx, dy);
    }

    public void setBounds (Rectangle rectangle) {
        actor.setBounds(rectangle.getX(), rectangle.getY(), rectangle.getWidth(), rectangle.getHeight());
    }

    public void setPosition (Vector2 newPos) {
        setX(newPos.x);
        setY(newPos.y);
        position.set(newPos);
    }

    public Vector2 getPosition () {
        position.x = getX();
        position.y = getY();

        return position;
    }

    public Vector2 getCenteredPosition () {
        return new Vector2(getX() + getWidth() / 2, getY() + getHeight() / 2);
    }

    public Rectangle getBounds () {
        // TODO: Take into account scale?
        return new Rectangle(getX(), getY(), getWidth(), getHeight());
    }

    public void setRotation (float degrees) {
        actor.setRotation(degrees);
    }

    public void draw (SpriteBatch batch) {
        Color color = actor.getColor();
        batch.setColor(color.r, color.g, color.b, color.a * alpha);
        batch.draw(
                textureRegion,
                getX(), getY(),
                getOriginX(), getOriginY(),
                getWidth(), getHeight(),
                getScaleX(), getScaleY(),
                getRotation()
        );

    }
    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }


    @Override
    public void reset () {
        actor.clear();
        actor.setBounds(-100, -100, 0, 0);
        position.setZero();
        textureRegion = null;
        actor.setRotation(0);
        alpha = 1f;
    }


}
