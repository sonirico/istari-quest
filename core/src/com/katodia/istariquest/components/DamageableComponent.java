package com.katodia.istariquest.components;

import com.artemis.PooledComponent;

public class DamageableComponent extends PooledComponent {

    private int currentHealth, totalHealth = 0;

    public DamageableComponent () {}

    public DamageableComponent (int health) {
        this.currentHealth = health;
        this.totalHealth = health;
    }

    public void setHealth (int health) {
        totalHealth = health;
        currentHealth = health;
    }

    public int getTotalHealth() {
        return totalHealth;
    }

    public int getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(int currentHealth) {
        this.currentHealth = currentHealth;
    }

    public void receivesDamage (int damage) {
        currentHealth -= damage;
    }

    @Override
    public void reset () {
        this.currentHealth = 0;
        this.totalHealth = 0;
    }
}
