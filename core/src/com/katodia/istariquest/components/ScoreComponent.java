package com.katodia.istariquest.components;


import com.artemis.PooledComponent;

public class ScoreComponent extends PooledComponent {

    private int points = 0;

    public ScoreComponent setPoints (int points) {
        this.points = points;

        return this;
    }

    public int getPoints() {
        return points;
    }

    public ScoreComponent add (int extraPoints) {
        this.points += extraPoints;

        return this;
    }

    public ScoreComponent subtract (int subtrahendPoints) {
        this.points -= subtrahendPoints;

        return this;
    }

    @Override
    protected void reset() {
        points = 0;
    }
}
