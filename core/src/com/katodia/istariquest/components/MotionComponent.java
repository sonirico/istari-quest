package com.katodia.istariquest.components;

import com.artemis.PooledComponent;
import com.badlogic.gdx.math.Vector2;


public class MotionComponent extends PooledComponent{

    public Vector2 velocity;

    public MotionComponent() {
        velocity = new Vector2();
    }

    public MotionComponent(float vx, float vy) {
        this.velocity = new Vector2(vx, vy);
    }

    public void setXY (float x, float y) {
        velocity.x = x;
        velocity.y = y;
    }

    public void setX (float x) {
        velocity.x = x;
    }

    public void setY (float y) {
        velocity.y = y;
    }

    public void set (Vector2 newVel) {
        velocity.set(newVel);
    }

    public void setZero () {
        velocity.setZero();
    }

    @Override
    public void reset () {
        velocity.setZero();
    }
}
