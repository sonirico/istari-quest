package com.katodia.istariquest.components.animations;

public final class AnimationValues {
    
    public enum STATES {
        WALKING,
        ATTACKING,
        PAUSED,
        DYING,
        DEATH,
        SHOT,
        CRASHED,

        // Gandalf specific
        SMOKING,
        SPEAKING_BUTTERFLY,
        UDUN_FLAME,
        LIGHTNING_STRIKE,
        LIGHTNING_SWING,
        SHIELDING,  
    };

    public enum BULLETS {
        UDUN_FLAME,
        UDUN_FLAME2,
        SPINNING_FLAME,
        DYING_FLAME_NORMAL
    }
    
}
