package com.katodia.istariquest.components.animations;


import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public final class AnimationBuilder {

    private int animationId;
    private int rowIndex;
    private int length;
    private float frameRate;
    private Animation.PlayMode playMode;

    AnimationBuilder() {

    }

    AnimationBuilder (
            Enum alias, int rowIndex, int length, float frameRate, Animation.PlayMode playMode
    ) {
        this.animationId = alias.ordinal();
        this.rowIndex = rowIndex;
        this.length = length;
        this.frameRate = frameRate;
        this.playMode = playMode;
    }

    public Animation.PlayMode getPlayMode() {
        return playMode;
    }

    AnimationBuilder setPlayMode(Animation.PlayMode playMode) {
        this.playMode = playMode;

        return this;
    }

    public int getAnimationId() {
        return animationId;
    }

    AnimationBuilder setAnimationId(Enum animationId) {
        this.animationId = animationId.ordinal();

        return this;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    AnimationBuilder setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;

        return this;
    }

    public int getLength() {
        return length;
    }

    AnimationBuilder setLength(int length) {
        this.length = length;

        return this;
    }

    public float getFrameRate() {
        return frameRate;
    }

    AnimationBuilder setFrameRate(float frameRate) {
        this.frameRate = frameRate;

        return this;
    }

    public Animation build (TextureRegion []  regions){
        Animation animation = new Animation(frameRate, regions);
        if (null != playMode) {
            animation.setPlayMode(playMode);
        }

        animation.setFrameDuration(this.frameRate);

        return animation;
    }
}
