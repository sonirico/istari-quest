package com.katodia.istariquest.components.animations;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.utils.Array;


public enum AnimationE {

    DYING_FLAME_NORMAL (AnimationValues.BULLETS.DYING_FLAME_NORMAL, "Dying Flame Normal.png", 32, 32) {
        @Override
        public void init() {
            animations.add(new AnimationBuilder()
                    .setRowIndex(0)
                    .setLength(4)
                    .setFrameRate(1/12.f)
                    .setAnimationId(AnimationValues.BULLETS.DYING_FLAME_NORMAL)
                    .setPlayMode(Animation.PlayMode.LOOP));
        }
    },

    UDUN_FLAME (AnimationValues.BULLETS.UDUN_FLAME, "bullets/Udun Flame.png", 32, 32) {
        @Override
        public void init() {
            animations.add(new AnimationBuilder()
                    .setRowIndex(0)
                    .setLength(5)
                    .setFrameRate(1/12.f)
                    .setAnimationId(AnimationValues.BULLETS.UDUN_FLAME)
                    .setPlayMode(Animation.PlayMode.LOOP));
        }
    },

    UDUN_FLAME_3 (AnimationValues.BULLETS.UDUN_FLAME, "bullets/Udun Flame 3.png", 32, 32) {
        @Override
        public void init() {
            animations.add(new AnimationBuilder()
                    .setRowIndex(0)
                    .setLength(5)
                    .setFrameRate(1/12.f)
                    .setAnimationId(AnimationValues.BULLETS.UDUN_FLAME)
                    .setPlayMode(Animation.PlayMode.LOOP_PINGPONG));
        }
    },

    SPINNING_FLAME (AnimationValues.BULLETS.SPINNING_FLAME, "bullets/Spinning Flame.png", 32, 32) {
        @Override
        public void init() {
            animations.add(new AnimationBuilder()
                    .setRowIndex(0)
                    .setLength(4)
                    .setFrameRate(1/10.f)
                    .setAnimationId(AnimationValues.BULLETS.SPINNING_FLAME)
                    .setPlayMode(Animation.PlayMode.LOOP));
        }
    },

    // ENEMIES

    BAT (AnimationValues.STATES.WALKING, "Bat 1.png", 32, 32) {
        @Override
        public void init() {
            animations.add(
                    new AnimationBuilder()
                    .setAnimationId(AnimationValues.STATES.WALKING)
                    .setRowIndex(0)
                    .setLength(2)
                    .setFrameRate(.1f)
                    .setPlayMode(Animation.PlayMode.LOOP)
            );
        }
    },

    ENT (AnimationValues.STATES.WALKING, "Ent.png", 48, 48) {
        @Override
        void init() {
            animations.add(new AnimationBuilder(AnimationValues.STATES.WALKING, 0, 2, 1, Animation.PlayMode.LOOP));
            animations.add(new AnimationBuilder(AnimationValues.STATES.SHOT, 1, 2, .5f, Animation.PlayMode.LOOP));
            animations.add(new AnimationBuilder(AnimationValues.STATES.ATTACKING, 2, 3, .33f, Animation.PlayMode.LOOP));
        }
    },

    SPIDER (AnimationValues.STATES.WALKING, "Spider.png", 32, 32) {
        @Override
        void init() {
            animations.add(new AnimationBuilder(AnimationValues.STATES.WALKING, 0, 2, .1f, Animation.PlayMode.LOOP));
            animations.add(new AnimationBuilder(AnimationValues.STATES.ATTACKING, 0, 2, .1f, Animation.PlayMode.LOOP));
        }
    },

    SKELETON_NORMAL (AnimationValues.STATES.WALKING, "skeleton normal.png", 32, 32) {
        @Override
        public void init() {
            animations.add(new AnimationBuilder(AnimationValues.STATES.WALKING, 1, 2, .2f, Animation.PlayMode.LOOP));
            animations.add(new AnimationBuilder(AnimationValues.STATES.ATTACKING, 2, 2, .4f, Animation.PlayMode.LOOP));
        }
    },

    SKELETON_MELEE (AnimationValues.STATES.WALKING, "skeleton melee.png", 32, 32) {
        @Override
        public void init() {
            animations.add(new AnimationBuilder(AnimationValues.STATES.WALKING, 1, 2, .2f, Animation.PlayMode.LOOP));
            animations.add(new AnimationBuilder(AnimationValues.STATES.ATTACKING, 2, 2, .4f, Animation.PlayMode.LOOP));
        }
    },

    SKELETON_ARCHER (AnimationValues.STATES.WALKING, "skeleton archer.png", 32, 32) {
        @Override
        public void init() {
            animations.add(new AnimationBuilder(AnimationValues.STATES.WALKING, 1, 2, .2f, Animation.PlayMode.LOOP));
            animations.add(new AnimationBuilder(AnimationValues.STATES.ATTACKING, 2, 2, .4f, Animation.PlayMode.LOOP));
        }
    },

    ORC_MELEE_1 (AnimationValues.STATES.WALKING, "Orc Melee 1.png", 32, 32) {
        @Override
        public void init() {
            animations.add(new AnimationBuilder(AnimationValues.STATES.WALKING, 0, 2, .2f, Animation.PlayMode.LOOP));
            animations.add(new AnimationBuilder(AnimationValues.STATES.ATTACKING, 1, 2, .4f, Animation.PlayMode.LOOP));
        }
    },

    ORC_CROSSBOW_1 (AnimationValues.STATES.WALKING, "Orc Crossbow 1.png", 32, 32) {
        @Override
        public void init() {
            animations.add(new AnimationBuilder(AnimationValues.STATES.WALKING, 0, 2, .2f, Animation.PlayMode.LOOP));
            animations.add(new AnimationBuilder(AnimationValues.STATES.ATTACKING, 1, 2, .5f, Animation.PlayMode.LOOP));
        }
    },

    // CHARACTERS

    RAGADAST (AnimationValues.STATES.DYING, "Dun Ragadast.png", 32, 32) {

        @Override
        public void init() {
            animations.add(new AnimationBuilder(AnimationValues.STATES.WALKING, 0, 2, .2f, Animation.PlayMode.LOOP));
            animations.add(new AnimationBuilder(AnimationValues.STATES.ATTACKING, 1, 2, .1f, Animation.PlayMode.LOOP));
        }
    },

    SARUMAN (AnimationValues.STATES.DYING, "White Saruman.png", 32, 32) {

        @Override
        public void init() {
            animations.add(new AnimationBuilder(AnimationValues.STATES.WALKING, 0, 2, .2f, Animation.PlayMode.LOOP));
            animations.add(new AnimationBuilder(AnimationValues.STATES.ATTACKING, 1, 2, .1f, Animation.PlayMode.LOOP));
        }
    },

    GANDALF (AnimationValues.STATES.WALKING, "Gandalf The Grey 3.png", 32, 32) {

        @Override
        public void init() {
            animations.add(new AnimationBuilder(AnimationValues.STATES.WALKING, 0, 2, .2f, Animation.PlayMode.LOOP));
            animations.add(new AnimationBuilder()
                    .setAnimationId(AnimationValues.STATES.ATTACKING)
                    .setRowIndex(1)
                    .setLength(2)
                    .setFrameRate(.1f)
                    .setPlayMode(Animation.PlayMode.LOOP)
            );
            animations.add(new AnimationBuilder()
                    .setAnimationId(AnimationValues.STATES.SMOKING)
                    .setRowIndex(2)
                    .setLength(2).setFrameRate(1)
                    .setPlayMode(Animation.PlayMode.LOOP)
            );
            animations.add(new AnimationBuilder()
                    .setAnimationId(AnimationValues.STATES.SPEAKING_BUTTERFLY)
                    .setRowIndex(3)
                    .setLength(2).setFrameRate(0.25f)
                    .setPlayMode(Animation.PlayMode.LOOP)
            );
            animations.add(new AnimationBuilder()
                    .setAnimationId(AnimationValues.STATES.UDUN_FLAME)
                    .setRowIndex(5)
                    .setLength(2).setFrameRate(1)
                    .setPlayMode(Animation.PlayMode.LOOP)
            );
        }
    },

    // BULLETS

    ELF_ARROW (AnimationValues.STATES.SHOT, "bullets/Elf Arrow.png", 16, 8) {

        @Override
        public void init() {
            animations.add(new AnimationBuilder()
                    .setAnimationId(AnimationValues.STATES.SHOT)
                    .setRowIndex(0)
                    .setLength(1).setFrameRate(.1f)
                    .setPlayMode(Animation.PlayMode.LOOP)
            );
        }
    },


    WIDE_SHOT (AnimationValues.STATES.SHOT, "bullets/Magic Missile.png", 8, 8) {

        @Override
        public void init() {
            animations.add(new AnimationBuilder()
                    .setAnimationId(AnimationValues.STATES.SHOT)
                    .setRowIndex(0)
                    .setLength(2).setFrameRate(.1f)
                    .setPlayMode(Animation.PlayMode.LOOP)
            );
        }
    },

    MAGIC_MISSILE (AnimationValues.STATES.SHOT, "bullets/Magic Missile.png", 8, 8) {

        @Override
        public void init() {
            animations.add(new AnimationBuilder()
                    .setAnimationId(AnimationValues.STATES.SHOT)
                    .setRowIndex(0)
                    .setLength(2).setFrameRate(.1f)
                    .setPlayMode(Animation.PlayMode.LOOP)
            );
        }
    };

    // Enum member

    public final int defaultAnimation;

    public final String textureName;

    public final int frameWidth, frameHeight;

    public Array<AnimationBuilder> animations;

    AnimationE(Enum animationId, String textureName, int frameWidth, int frameHeight) {
        this.defaultAnimation = animationId.ordinal();
        this.textureName = textureName;
        this.frameWidth = frameWidth;
        this.frameHeight = frameHeight;
        this.animations = new Array();

        init();
    }

    abstract void init ();

    public String getTextureName () {
        return textureName;
    }
}

