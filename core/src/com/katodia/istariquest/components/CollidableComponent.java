package com.katodia.istariquest.components;

import com.artemis.PooledComponent;

public class CollidableComponent extends PooledComponent {

    public boolean hasCollided = false;

    @Override
    public void reset() {
        hasCollided = false;
    }
}
