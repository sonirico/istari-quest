package com.katodia.istariquest.components.weapons;

import com.badlogic.gdx.math.Vector2;

public class Shot {

    Vector2 spawnPoint;
    Vector2 direction;

    Shot (Vector2 spawnPoint, Vector2 direction) {
        this.spawnPoint = spawnPoint;
        this.direction = direction;
    }

    public Vector2 getSpawnPoint () {
        return spawnPoint;
    }

    public Vector2 getDirection () {
        return direction;
    }
}