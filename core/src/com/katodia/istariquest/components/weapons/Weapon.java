package com.katodia.istariquest.components.weapons;


import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.katodia.istariquest.G;
import com.katodia.istariquest.components.animations.AnimationE;

public enum Weapon {

    MAGIC_MISSILE () {
        @Override
        void init() {
            this.animation = AnimationE.MAGIC_MISSILE;
            this.groups = new String[]{G.GROUPS.BULLET, G.GROUPS.BULLET_FRIEND};
            this.delayBetweenShots = 1f;
            this.width = 8;
            this.height = 8;
            this.burst.add(new Shot(new Vector2(32, 8), new Vector2(200, 0)));
            this.burst.add(new Shot(new Vector2(32, 8), new Vector2(200, 50)));
            this.burst.add(new Shot(new Vector2(32, 8), new Vector2(200, -50)));
        }
    },

    WIDE_SHOT () {
        @Override
        void init() {
            this.animation = AnimationE.WIDE_SHOT;
            this.groups = new String[]{G.GROUPS.BULLET, G.GROUPS.BULLET_FRIEND};
            this.delayBetweenShots = .2f;
            this.width = 8;
            this.height = 8;
            this.burst.add(
                    new Shot(new Vector2(32, 8), new Vector2(200, 0))
            );
        }
    },

    ELF_ARROW () {
        @Override
        void init() {
            this.animation = AnimationE.ELF_ARROW;
            this.groups = new String[]{G.GROUPS.BULLET, G.GROUPS.BULLET_FRIEND};
            this.delayBetweenShots = .2f;
            this.width = 16;
            this.height = 8;
            this.burst.add(new Shot(new Vector2(32, 8), new Vector2(200, 0)));
        }
    };

    AnimationE animation;

    String [] groups;

    float delayBetweenShots;

    int width, height;

    Array<Shot> burst;

    Weapon() {

        this.burst = new Array<Shot>();

        init();
    }

    abstract void init ();

    public AnimationE getAnimation () {
        return animation;
    }

    public float getDelayBetweenShots() {
        return delayBetweenShots;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Array<Shot> getBurst() {
        return this.burst;
    }

    public String [] getGroups() {
        return groups;
    }




}

