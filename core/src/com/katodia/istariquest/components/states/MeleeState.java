package com.katodia.istariquest.components.states;

public class MeleeState extends StateComponent {

    private int damage = 0;

    public MeleeState() {

    }

    public int getDamage() {
        return damage;
    }

    public MeleeState setDamage(int damage) {
        this.damage = damage;

        return this;
    }

    @Override
    protected void reset() {
        damage = 0;
    }
}
