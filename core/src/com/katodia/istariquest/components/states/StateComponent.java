package com.katodia.istariquest.components.states;

import com.artemis.PooledComponent;

public abstract class StateComponent extends PooledComponent{

    protected Enum animationState;

    protected boolean initialized = false;

    public <T extends StateComponent> T setAnimationState (Enum animationState) {
        this.animationState = animationState;
        return (T) this;
    }

    public Enum getAnimationState () {
        return animationState;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public void setInitialized(Boolean b) {
        initialized = b;
    }

    @Override
    protected void reset() {
        initialized = false;
    }
}
