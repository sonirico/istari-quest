package com.katodia.istariquest.components.states.common;


import com.katodia.istariquest.components.states.StateComponent;

public class DyingState extends StateComponent {

    private float timeToFade = 0f;

    public float getTimeToFade() {
        return timeToFade;
    }

    public void setTimeToFade(float timeToFade) {
        this.timeToFade = timeToFade;
    }

    @Override
    protected void reset() {
        super.reset();
        timeToFade = 0f;
    }
}
