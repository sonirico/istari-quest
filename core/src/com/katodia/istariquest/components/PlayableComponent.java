package com.katodia.istariquest.components;


import com.artemis.PooledComponent;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.msg.Telegraph;
import com.katodia.istariquest.systems.input.VirtualBoard;

public class PlayableComponent extends PooledComponent implements Telegraph {

    public enum COMMANDS {
        MOVE_LEFT, MOVE_RIGHT, MOVE_UP, MOVE_DOWN,
        FIRE,
        UDUN_FLAME, // Temporal TODO: Erase this
        EAGLE_CALL // Temporal TODO: Erase this
    };

    private VirtualBoard virtualBoard = null;

    public PlayableComponent () {

        /*

        MessageManager.getInstance().addListener(this, MessageTypes.PLAYER_MOVE_LEFT);
        MessageManager.getInstance().addListener(this, MessageTypes.PLAYER_MOVE_RIGHT);
        MessageManager.getInstance().addListener(this, MessageTypes.PLAYER_MOVE_UP);
        MessageManager.getInstance().addListener(this, MessageTypes.PLAYER_MOVE_DOWN);*/
    }

    public void setVirtualBoard(VirtualBoard virtualBoard) {
        this.virtualBoard = virtualBoard;

    }

    public VirtualBoard getVirtualBoard () {
        return this.virtualBoard;
    }


    @Override
    public boolean handleMessage(Telegram msg) {
        /*
        this.moveLeft = msg.message == MessageTypes.PLAYER_MOVE_LEFT;
        this.moveRight = msg.message == MessageTypes.PLAYER_MOVE_RIGHT;
        this.moveUp = msg.message == MessageTypes.PLAYER_MOVE_UP;
        this.moveDown = msg.message == MessageTypes.PLAYER_MOVE_DOWN;
*/
        // No longer needed
        //msg.reset();

        return true;
    }

    public void resetBoard () {
        for (Enum command : COMMANDS.values()) {
            this.virtualBoard.put(command.ordinal(), false);
        }
    }

    @Override
    public void reset () {
        virtualBoard.reset();
    }

}

