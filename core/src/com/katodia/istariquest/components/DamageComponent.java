package com.katodia.istariquest.components;

import com.artemis.PooledComponent;

public class DamageComponent extends PooledComponent{

    public int damage = 0;

    public DamageComponent () {}

    public DamageComponent (int damage) {
        this.damage = damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public void reset () {
        damage = 0;
    }
}
