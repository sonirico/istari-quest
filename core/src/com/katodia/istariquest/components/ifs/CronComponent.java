package com.katodia.istariquest.components.ifs;

public interface CronComponent {

    void tick(float deltaTime);

    void resetTimer();

    float getTimer();
}
