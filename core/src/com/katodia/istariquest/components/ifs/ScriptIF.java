package com.katodia.istariquest.components.ifs;


import com.artemis.Entity;

public interface ScriptIF {

    void inserted (Entity e);

    void process (Entity e);

    void removed (Entity e);

}
