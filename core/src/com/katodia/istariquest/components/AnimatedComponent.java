package com.katodia.istariquest.components;

import com.artemis.PooledComponent;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.IntMap;
import com.katodia.istariquest.AssetsHolder;
import com.katodia.istariquest.components.animations.AnimationBuilder;
import com.katodia.istariquest.components.animations.AnimationE;
import com.katodia.istariquest.components.ifs.CronComponent;


public class AnimatedComponent extends PooledComponent implements CronComponent {

    private boolean initialized = false;

    private transient float elapsedTime = 0f;

    private transient IntMap<Animation> animations;

    private transient Animation currentAnimation;

    private AnimationE animationE;

    public boolean startAtRandomFrame = false;

    public boolean flipX = false, flipY = false;

    public AnimatedComponent () {
        this.animations = new IntMap<>();
    }

    public AnimatedComponent setAnimation(AnimationE aE) {
        animationE = aE;
        initialized = false;

        return this;
    }

    public void initialize() {
        if (initialized) throw new RuntimeException("Are you trying to reinitialize the same animation?");

        Texture texture = AssetsHolder.get(animationE.getTextureName(), Texture.class);
        TextureRegion[][] tmp = TextureRegion.split(
                texture,
                animationE.frameWidth,
                animationE.frameHeight
        );

        for (AnimationBuilder ab: animationE.animations) {

            TextureRegion [] rowRegions = new TextureRegion[ab.getLength()];

            for (int i = 0, l = ab.getLength(); l > i; i++) {
                rowRegions[i] = tmp[ab.getRowIndex()][i];
            }

            animations.put(
                ab.getAnimationId(),
                ab.build(rowRegions)
            );
        }

        this.setCurrentAnimation(animationE.defaultAnimation);
        initialized = true;
    }

    public Animation getCurrentAnimation() {
        return currentAnimation;
    }

    public boolean hasAnimation (Enum animation) {
        return animations.containsKey(animation.ordinal());
    }

    public boolean hasAnimation (int animationId) {
        return animations.containsKey(animationId);
    }

    public int getDefaultAnimation () {
        return animationE.defaultAnimation;
    }

    public void setDefaultAnimation () {
        setCurrentAnimation(animationE.defaultAnimation);
    }

    public void setCurrentAnimation(int animationId) {
        if (! hasAnimation(animationId) ) {
            throw new RuntimeException(animationId + " does not exists in the animation map.");
        }

        currentAnimation = animations.get(animationId);

        // Whether we should start at frame 0 or not.
        if (! startAtRandomFrame) {
            elapsedTime = 0f;
        } else {
            setRandomFrame();
        }
        // Shall we flip the sprites
        if (flipX || flipY) {
            flip(flipX, flipY);
        }
    }

    public void tick (float dt) {
        elapsedTime += dt;
    }

    @Override
    public void resetTimer() {
        elapsedTime = 0f;
    }

    @Override
    public float getTimer() {
        return elapsedTime;
    }

    public void setCurrentAnimation(Animation newAnimation) {
        this.currentAnimation = newAnimation;
    }

    public boolean isInitialized () {
        return initialized;
    }

    public void setAnimation (Animation a) {
        this.currentAnimation = a;
    }

    public Animation getAnimation () {
        return this.currentAnimation;
    }

    public float getFrameRate() {
        return currentAnimation.getFrameDuration();
    }

    public float getRowFrameDuration (Enum animationId) {
        if (! hasAnimation(animationId)) {
            throw new RuntimeException("The specified animation ID doest not exist");
        }
        return animations.get(animationId.ordinal()).getAnimationDuration();
    }

    public float getAnimationDuration () {
        return currentAnimation.getAnimationDuration();// getFrameRate() * currentAnimation.getKeyFrames().length;
    }

    public TextureRegion getKeyFrame () {
        return currentAnimation.getKeyFrame(elapsedTime);
    }

    public void setPlayMode (Animation.PlayMode mode) {
        currentAnimation.setPlayMode(mode);
    }

    public void flip (boolean x, boolean y) {
    	animations.values().forEach((animation) -> {
			for (TextureRegion tr: animation.getKeyFrames()) {
				tr.flip(x, y);
			}
		});
    }

    public void setRandomFrame () {
        elapsedTime = MathUtils.random(0, getAnimationDuration());
    }

    @Override
    protected void reset() {
        initialized = false;
        startAtRandomFrame = false;
        flipX = flipY = false;
        animations.clear();
        currentAnimation = null;
        elapsedTime = 0f;
    }

}
