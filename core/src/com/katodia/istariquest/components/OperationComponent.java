package com.katodia.istariquest.components;


import com.artemis.Entity;
import com.artemis.PooledComponent;
import com.artemis.World;
import se.feomedia.orion.Operation;

public class OperationComponent extends PooledComponent {

    public Operation operation = null;

    public boolean registered = false;

    public OperationComponent () {

    }

    public void setOperation (Operation operation) {
        this.operation = operation;
    }

    public void register (Entity entity) {
        operation.register(entity);
    }

    public void register (World world, int entityId) {
        operation.register(world, entityId);
    }

    @Override
    protected void reset() {
        operation = null;
        registered = false;
    }
}
