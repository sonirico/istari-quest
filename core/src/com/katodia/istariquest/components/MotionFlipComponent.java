package com.katodia.istariquest.components;

import com.artemis.PooledComponent;
import com.badlogic.gdx.math.Vector2;

public class MotionFlipComponent extends PooledComponent {

    /**
     * Flags which have to be manually enabled for entity to be able to flip
     * in the specified axis.
     */

    public boolean flipX = false;

    public boolean flipY = false;

    /**
     * Records the last known velocity vector.
     */

    public Vector2 lastVel = Vector2.Zero.cpy();

    @Override
    protected void reset() {
        flipX = false;
        flipY = false;
        lastVel.setZero();
    }
}
