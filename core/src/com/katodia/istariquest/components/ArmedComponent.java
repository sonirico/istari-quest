package com.katodia.istariquest.components;

import com.artemis.PooledComponent;
import com.katodia.istariquest.components.ifs.CronComponent;
import com.katodia.istariquest.components.weapons.Weapon;

public class ArmedComponent extends PooledComponent implements CronComponent{

    private boolean actionRequested;

    private float timer;

    private Weapon currentWeapon;

    public ArmedComponent () {
    }

    public void tick ( float dt ) {
        timer += dt;
    }

    public void resetTimer () {
        timer = 0;
    }

    public float getTimer () {
        return timer;
    }

    public void requestAction() {
        actionRequested = true;
    }

    public boolean isActionRequested() {
        return actionRequested;
    }

    public boolean canTrigger() {
        if ( null == currentWeapon ) return false;
        return timer >= currentWeapon.getDelayBetweenShots();
    }

    public Weapon getCurrentWeapon () {
        return currentWeapon;
    }

    public void clearActionRequest() {
        actionRequested = false;
    }

    public void setCurrentWeapon (Weapon weapon) {
        currentWeapon = weapon;
        // The first shot should not be delayed
        timer = currentWeapon.getDelayBetweenShots();
    }

    @Override
    public void reset () {
        this.actionRequested = false;
        resetTimer();
        this.currentWeapon = null;
    }
}
