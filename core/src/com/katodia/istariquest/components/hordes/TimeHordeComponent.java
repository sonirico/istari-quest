package com.katodia.istariquest.components.hordes;


import com.katodia.istariquest.components.ifs.CronComponent;

import java.util.HashMap;
import java.util.Map;

public class TimeHordeComponent extends HordeComponent implements CronComponent{

    private float timer = 0f;

    /**
     * This is intended to provide more/less score depending on how long it took the player
     * to destroy all the elements
     */

    private Map<Float, Integer> pointsPerTime;

    public TimeHordeComponent () {
        pointsPerTime = new HashMap<>();
    }

    public TimeHordeComponent setMilestone (float time, int points) {
        pointsPerTime.put(time, points);
/*
        SortedSet<Float> keys = new TreeSet<>(pointsPerTime.keySet());
        for (float key : keys) {
            float value = pointsPerTime.get(key);
            // do something
        }
        */

        return this;
    }

    public int getPoints () {
        for (Map.Entry<Float, Integer> entry: pointsPerTime.entrySet()) {
            if (timer < entry.getKey()) {
                return entry.getValue();
            }
        }

        return 0;
    }

    @Override
    public void tick(float deltaTime) {
        timer += deltaTime;
    }

    @Override
    public void resetTimer() {
        timer = 0;
    }

    @Override
    public float getTimer() {
        return timer;
    }

    @Override
    protected void reset() {
        super.reset();
        resetTimer();
        pointsPerTime.clear();
    }

}
