package com.katodia.istariquest.components.hordes;


import com.artemis.Entity;
import com.artemis.PooledComponent;
import com.artemis.annotations.EntityId;
import com.artemis.annotations.LinkPolicy;
import com.artemis.annotations.PooledWeaver;
import com.artemis.utils.IntBag;

import static com.artemis.annotations.LinkPolicy.Policy.CHECK_SOURCE_AND_TARGETS;

@PooledWeaver
public class HordeComponent extends PooledComponent {

	@EntityId @LinkPolicy(CHECK_SOURCE_AND_TARGETS)
	public IntBag entityIds = new IntBag();

	public int totalSize = -1;

	public HordeComponent with (Entity e) {
		if (entityIds.contains(e.getId())) {
			throw new RuntimeException("This horde has this soldier already");
		}

		entityIds.add(e.getId());

		return this;
	}

	public HordeComponent march () {
		totalSize = entityIds.size();
		return this;
	}

	public int getTotalSize() {
		return totalSize;
	}

	public int size () {
		return entityIds.size();
	}

	public boolean isEmpty () {
		return size() <= 0;
	}

	@Override
	protected void reset() {
		entityIds.clear();
	}
}
