package com.katodia.istariquest.components;

import com.artemis.PooledComponent;
import com.katodia.istariquest.components.ifs.CronComponent;


public class LifeSpanComponent extends PooledComponent implements CronComponent {

    private float timeToBeRemoved = 0f; // Seconds before being removed when culled

    private float timer = 0f;

    public LifeSpanComponent () {

    }

    public float getTimeToBeRemoved() {
        return timeToBeRemoved;
    }

    public void setTimeToBeRemoved(float timeToBeRemoved) {
        this.timeToBeRemoved = timeToBeRemoved;
    }

    @Override
    protected void reset() {
        timeToBeRemoved = 0f;
        resetTimer();
    }

    @Override
    public void tick(float deltaTime) {
        timer += deltaTime;
    }

    @Override
    public void resetTimer() {
        timer = 0f;
    }

    @Override
    public float getTimer() {
        return timer;
    }
}
