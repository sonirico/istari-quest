package com.katodia.istariquest.components;

import com.artemis.PooledComponent;

public class SpawnableComponent extends PooledComponent{

    private float distanceToAppear = 0f;

    public void setDistanceToAppear (float d) {
        this.distanceToAppear = d;
    }

    public boolean canSpawn (float distanceTravelled) {
        return distanceToAppear <= distanceTravelled;
    }

    @Override
    public void reset () {
        distanceToAppear = 0;
    }
}
