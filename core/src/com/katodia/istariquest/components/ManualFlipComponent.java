package com.katodia.istariquest.components;

import com.artemis.PooledComponent;

public class ManualFlipComponent extends PooledComponent {

    /**
     * Flags which have to be manually enabled for entity to be able to flip
     * in the specified axis.
     */

    public boolean flipX = false;

    public boolean flipY = false;

    @Override
    protected void reset() {
        flipX = false;
        flipY = false;
    }
}
